CREATE DATABASE IF NOT EXISTS EmailClient ;

USE EmailClient;

DROP USER IF EXISTS obied@'localhost';
CREATE USER obied@'localhost' IDENTIFIED BY 'obiedStandard';
GRANT ALL ON EmailClient.* TO obied@'localhost';

-- This creates a user with access from any IP number except localhost
-- Use only if your MySQL database is on a different host from localhost

FLUSH PRIVILEGES;