package com.obied.persistence;

import com.obied.beans.EmailBean;
import com.obied.beans.javaFX.FolderBean;
import com.obied.exception.CannotRenameFolderException;
import com.obied.exception.FolderAlreadyExistsException;
import com.obied.exception.InvalidDeleteFolderException;
import java.sql.SQLException;
import java.util.List;

/**
 * Interface with methods to be able to create, read, update and delete
 *
 * @author Obied
 */
public interface EmailDAO {

    // Create
    public int createEmail(EmailBean emailBean, int folderKey) throws SQLException;

    public int createFolder(String FolderName) throws SQLException, FolderAlreadyExistsException;
   
    
    // Read
    public EmailBean findById(int id) throws SQLException;

    public List<EmailBean> findAll() throws SQLException;

    public List<EmailBean> findFields(String emailAddress) throws SQLException;

    public List<EmailBean> findFolder(int id) throws SQLException;

    public List<FolderBean> findAllFolders() throws SQLException;
    
    public int findFolderId(String folderName) throws SQLException;

    // Update    
    public int updateFolder(EmailBean emailBean) throws SQLException;

    public int updateDraft(EmailBean emailBean) throws SQLException;

    public int renameFolder(String oldFolderName, String newFolderName) throws SQLException, CannotRenameFolderException, FolderAlreadyExistsException;

    // Delete
    public int delete(int id) throws SQLException;

    public int deleteFolder(String folderName) throws SQLException, InvalidDeleteFolderException;
    

}
