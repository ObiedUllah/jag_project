package com.obied.persistence;

import com.obied.beans.EmailBean;
import com.obied.beans.MailConfigBean;
import com.obied.beans.javaFX.FolderBean;
import com.obied.exception.CannotRenameFolderException;
import com.obied.exception.FolderAlreadyExistsException;
import com.obied.exception.InvalidDeleteFolderException;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.SQLIntegrityConstraintViolationException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import jodd.mail.EmailAddress;
import jodd.mail.EmailAttachment;
import jodd.mail.EmailMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This class implements the EmailDAO interface. It makes the database CRUD
 * possible. It allows actions like creating an email, finding emails or
 * folders, updating drafts and folders and deleting emails.
 *
 * @author Obied
 */
public class EmailDAOimpl implements EmailDAO {

    private final static Logger LOG = LoggerFactory.getLogger(EmailDAOimpl.class);

    private final MailConfigBean mailConfigBean;
    private final String mySqlUrl;
    private final String selectQuery;

    /**
     * This intitializes the values of mailconfigbean and also a sql string
     * often used for finding
     *
     * @param mailConfigBean
     */
    public EmailDAOimpl(MailConfigBean mailConfigBean) {
        this.mailConfigBean = mailConfigBean;
        this.mySqlUrl = mailConfigBean.getMysqlURL() + ":" + mailConfigBean.getMysqlPort() + "/" + mailConfigBean.getMysqlDatabase();
        //added this because it was repeated three times
        selectQuery = "select distinct EmailId, FolderId, FromAddress, Subject, TextMessage, HtmlMessage, ReceivedDate, SendDate from Email"
                + " left outer join EmailToAddress using(EmailId)"
                + " left outer join Address on EmailToAddress.RecipientEmailAddressId = Address.EmailAddressId"
                + " left outer join Attachment using(EmailId)";
    }


    /* CREATE  ************************************************/
    /**
     * this method inserts an email into the email table while creating records
     * for the bridging tables such as EmailToAddress and Attachments through
     * helper methods
     *
     * @param emailBean that we will create a record from
     * @param folderKey
     * @return int the amount of records created
     * @throws SQLException
     */
    @Override
    public int createEmail(EmailBean emailBean, int folderKey) throws SQLException {
        int result;
        String createQuery = "insert into Email(FolderId,FromAddress, Subject, TextMessage, HtmlMessage, SendDate, ReceivedDate)"
                + " values(?,?,?,?,?,?,?)";
        //create connection 
        try ( Connection connection = DriverManager.getConnection(this.mySqlUrl, mailConfigBean.getMysqlUser(), mailConfigBean.getMysqlPassword()); // Using a prepared statement to handle the conversion
                // of special characters in the SQL statement and guard against
                // SQL Injection 
                  PreparedStatement ps = connection.prepareStatement(createQuery, Statement.RETURN_GENERATED_KEYS);) {
            fillPreparedStatementForEmail(ps, emailBean, folderKey);
            result = ps.executeUpdate();

            // Retrieve generated primary key value and assign to bean
            try ( ResultSet rs = ps.getGeneratedKeys();) {
                int recordNum = -1;
                if (rs.next()) {
                    recordNum = rs.getInt(1);
                }
                emailBean.setId(recordNum);

                LOG.debug("New email record ID is " + recordNum);
            }

            //create the values for the bridging table EmailToAddress
            //also makes sure the id from the emailbean is the same
            createEmailToAddressTO(emailBean);
            createEmailToAddressCC(emailBean);
            createEmailToAddressBCC(emailBean);

            //create the values for the bridging table Attachment
            createAttachment(emailBean);

            //add the attachment to the root of the project
            this.saveBlobToDisk(emailBean.getId());
        }
        LOG.info("# of emails inserted : " + result);
        return result;
    }

    /**
     * This method allows the creation of a brand new folder If the folder is
     * not new, then an error is thrown through a helper method
     *
     * @param folderName the foldername to be created
     * @return int the amount of records created
     * @throws SQLException
     * @throws com.obied.exception.FolderAlreadyExistsException
     */
    @Override
    public int createFolder(String folderName) throws SQLException, FolderAlreadyExistsException {
        int result = -1;
        String createQuery = "insert into Folder(FolderName) values(?)";
        //create connection 
        try ( Connection connection = DriverManager.getConnection(this.mySqlUrl, mailConfigBean.getMysqlUser(), mailConfigBean.getMysqlPassword()); // Using a prepared statement to handle the conversion
                // of special characters in the SQL statement and guard against
                // SQL Injection 
                  PreparedStatement ps = connection.prepareStatement(createQuery, Statement.RETURN_GENERATED_KEYS);) {
            ps.setString(1, folderName);
            try {
                result = ps.executeUpdate();
            } catch (SQLIntegrityConstraintViolationException exception) {
                throw new FolderAlreadyExistsException("THE FOLDER ALREADY EXISTS");
            }
        }
        LOG.info("# of Folders inserted : " + result);
        return result;
    }

    /**
     * this will create an attachment for every attachment in an email embedded
     * or regular
     *
     * @param emailBean that we will create a record from
     * @throws SQLException
     */
    private void createAttachment(EmailBean emailBean) throws SQLException {
        LOG.debug("CREATING THE ATTACHMENTS IN THE CREATATTACHMENT METHOD");
        int result;
        String createQuery = "insert into attachment(EmailId, FileName, ContentId, Attachment) values(?,?,?,?)";
        //go thruogh every attachment
        for (EmailAttachment email : emailBean.email.attachments()) {
            LOG.debug("inside the for loop");
            //create connection 
            try ( Connection connection = DriverManager.getConnection(this.mySqlUrl, mailConfigBean.getMysqlUser(), mailConfigBean.getMysqlPassword()); // Using a prepared statement to handle the conversion
                    // of special characters in the SQL statement and guard against
                    // SQL Injection 
                      PreparedStatement ps = connection.prepareStatement(createQuery, Statement.RETURN_GENERATED_KEYS);) {
                fillPreparedStatementForAttachment(ps, emailBean, email);
                result = ps.executeUpdate();
            }
            LOG.info("# of attachment inserted: " + result);
        }
        //the reason why i do not return the result its because it will only return the result of the last email in the foreach
    }

    /**
     * this will create email addresses for the to field and inserts into the
     * table
     *
     * @param emailBean that we will create a record from
     * @throws SQLException
     */
    private void createEmailToAddressTO(EmailBean emailBean) throws SQLException {
        int result;

        String createQuery = "insert into EmailToAddress(EmailId, RecipientEmailAddressId, Category) values(?,?,?)";
        //go through all the to emails
        for (EmailAddress email : emailBean.email.to()) {
            //create connection 
            try ( Connection connection = DriverManager.getConnection(this.mySqlUrl, mailConfigBean.getMysqlUser(), mailConfigBean.getMysqlPassword()); // Using a prepared statement to handle the conversion
                    // of special characters in the SQL statement and guard against
                    // SQL Injection 
                      PreparedStatement pstatement = connection.prepareStatement(createQuery, Statement.RETURN_GENERATED_KEYS);) {
                fillPreparedStatementForEmailToAddress(pstatement, emailBean, email, "TO");

                result = pstatement.executeUpdate();
            }
            LOG.info("# of emailToAddress inserted: " + result);
        }
        //the reason why i do not return the result its because it will only return the result of the last email in the foreach

    }

    /**
     * this will create email addresses for the cc field and inserts into the
     * table
     *
     * @param emailBean that we will create a record from
     * @throws SQLException
     */
    private void createEmailToAddressCC(EmailBean emailBean) throws SQLException {
        int result;

        String createQuery = "insert into EmailToAddress(EmailId, RecipientEmailAddressId, Category) values(?,?,?)";

        //go through all the cc emails
        for (EmailAddress email : emailBean.email.cc()) {
            //create connection 
            try ( Connection connection = DriverManager.getConnection(this.mySqlUrl, mailConfigBean.getMysqlUser(), mailConfigBean.getMysqlPassword()); // Using a prepared statement to handle the conversion
                    // of special characters in the SQL statement and guard against
                    // SQL Injection 
                      PreparedStatement pstatement = connection.prepareStatement(createQuery, Statement.RETURN_GENERATED_KEYS);) {
                fillPreparedStatementForEmailToAddress(pstatement, emailBean, email, "CC");
                result = pstatement.executeUpdate();
            }
            LOG.info("# of emailToAddress inserted: " + result);
        }
    }

    /**
     * this will create email addresses for the bcc field and inserts into the
     * table
     *
     * @param emailBean that we will create a record from
     * @throws SQLException
     */
    private void createEmailToAddressBCC(EmailBean emailBean) throws SQLException {
        int result;

        String createQuery = "insert into EmailToAddress(EmailId, RecipientEmailAddressId, Category) values(?,?,?)";
        //go through all the bcc emails
        for (EmailAddress email : emailBean.email.bcc()) {
            //create connection 
            try ( Connection connection = DriverManager.getConnection(this.mySqlUrl, mailConfigBean.getMysqlUser(), mailConfigBean.getMysqlPassword()); // Using a prepared statement to handle the conversion
                    // of special characters in the SQL statement and guard against
                    // SQL Injection 
                      PreparedStatement pstatement = connection.prepareStatement(createQuery, Statement.RETURN_GENERATED_KEYS);) {
                fillPreparedStatementForEmailToAddress(pstatement, emailBean, email, "BCC");
                result = pstatement.executeUpdate();
            }
            LOG.info("# of emailToAddress inserted: " + result);
        }
    }

    /**
     * This insert a new Email Address into the Address table it reurns the id
     * of the current EmailAddress
     *
     * @param emailAddress create email address
     * @return id of the current emailAddress or -1 if something wrong happens
     * @throws SQLException
     */
    private int createAddress(String emailAddress) throws SQLException {
        int result;
        String createQuery = "insert into Address(emailAddress) values(?)";

        //create connection 
        try ( Connection connection = DriverManager.getConnection(this.mySqlUrl, mailConfigBean.getMysqlUser(), mailConfigBean.getMysqlPassword()); // Using a prepared statement to handle the conversion
                // of special characters in the SQL statement and guard against
                // SQL Injection 
                  PreparedStatement ps = connection.prepareStatement(createQuery, Statement.RETURN_GENERATED_KEYS);) {
            ps.setString(1, emailAddress);
            result = ps.executeUpdate();

            LOG.info("Email does not exist,# of Address inserted: " + result);

            // Retrieve generated primary key value and assign to bean
            try ( ResultSet rs = ps.getGeneratedKeys();) {
                int recordNum = -1;
                if (rs.next()) {
                    recordNum = rs.getInt(1);
                }
                LOG.debug("New Address record ID is " + recordNum);
                return recordNum;
            }
        }
    }

    /**
     * This method checks if an email exists. if it does exist, it returns its
     * id from the EmailAddress table If it does not exist , it will call a
     * method that will create an address and return its id
     *
     * @param emailAddress the address that needs to be checked if it exists
     * @return id of the current email address or -1 if something wrong happens
     * @throws SQLException
     */
    public int checkEmailExists(String emailAddress) throws SQLException {
        String newSelectQuery = "select EmailAddressId, EmailAddress from Address";
        try ( Connection connection = DriverManager.getConnection(this.mySqlUrl, mailConfigBean.getMysqlUser(), mailConfigBean.getMysqlPassword()); // Using a prepared statement to handle the conversion
                // of special characters in the SQL statement and guard against
                // SQL Injection 
                  PreparedStatement ps = connection.prepareStatement(newSelectQuery, Statement.RETURN_GENERATED_KEYS);) {
            try ( ResultSet resultSet = ps.executeQuery()) {
                while (resultSet.next()) {
                    if (resultSet.getString("EmailAddress").contentEquals(emailAddress)) {
                        LOG.info("Email already exists, current Address id: " + resultSet.getInt("EmailAddressId"));
                        return resultSet.getInt("EmailAddressId");
                    }
                }
            }
        }
        return createAddress(emailAddress);
    }

    /**
     * This will fill the prepared statement for an email
     *
     * @param ps prepared statement that needs to be filled
     * @param emailBean that we will create a record from
     * @throws SQLException
     */
    private void fillPreparedStatementForEmail(PreparedStatement ps, EmailBean emailBean, int folderid) throws SQLException {
        //make the email inbox, sent , draft
        ps.setInt(1, folderid);
        ps.setString(2, emailBean.email.from().getEmail());
        ps.setString(3, emailBean.email.subject());

        //get all messages and if its a html then add it to the html message
        for (EmailMessage message : emailBean.email.messages()) {
            if (message.getMimeType().equals("text/html")) {
                ps.setString(5, message.getContent());
            } else {
                ps.setString(4, message.getContent());
            }
        }

        checkEmailType(ps, folderid, emailBean);
    }

    /**
     * checks what kind of email is created
     *
     * @param ps
     * @param folderid
     * @param emailBean
     * @throws SQLException
     */
    private void checkEmailType(PreparedStatement ps, int folderid, EmailBean emailBean) throws SQLException {
        //email is being received
        if (folderid == 1) {
            LOG.debug("EMAIL IS BEING RECEIVED, TIME: " + Timestamp.valueOf(emailBean.getReceivedDate()));
            ps.setTimestamp(6, null);
            ps.setTimestamp(7, Timestamp.valueOf(emailBean.getReceivedDate()));
        }
        //email is being sent
        if (folderid == 2) {
            LOG.debug("EMAIL IS BEING SENT, TIME: " + new Timestamp(System.currentTimeMillis()));
            ps.setTimestamp(6, new Timestamp(System.currentTimeMillis()));
            ps.setTimestamp(7, null); //this email is not being received
        }
        //email is a draft
        if (folderid == 3) {
            LOG.debug("EMAIL IS A Draft NO TIME");
            ps.setTimestamp(6, null);
            ps.setTimestamp(7, null);
        }
    }

    /**
     * This will fill the prepared statements for the attachment table
     *
     * @param ps prepared statement that needs to be filled
     * @param emailBean that we will create a record from
     * @param email the email containing the attachments
     * @throws SQLException
     */
    private void fillPreparedStatementForAttachment(PreparedStatement ps, EmailBean emailBean, EmailAttachment email) throws SQLException {
        LOG.debug("FILLING ATTACHMENTS ");
        LOG.debug("ID EMAIL ATTCH " + emailBean.getId());
        LOG.debug("NAME EMAIL ATTACH: " + email.getName());
        //LOG.debug("the bytes: " + email.toByteArray());
        ps.setInt(1, emailBean.getId());
        ps.setString(2, email.getName());
        ps.setString(3, email.getContentId());
        ps.setBytes(4, email.toByteArray());

    }

    /**
     * This will fill the prepared statements for the emailToAddress table
     *
     * @param pstatement prepared statement that needs to be filled
     * @param emailBean that we will create a record from
     * @param email email containing the name of the email address
     * @param category the fields to, cc, bcc
     * @throws SQLException
     */
    private void fillPreparedStatementForEmailToAddress(PreparedStatement pstatement, EmailBean emailBean, EmailAddress email, String category) throws SQLException {
        pstatement.setInt(1, emailBean.getId());
        pstatement.setInt(2, checkEmailExists(email.getEmail()));
        pstatement.setString(3, category);
    }

    /* READ **********************************************************************************************************************/
    /**
     * this will return a specific EmailBean by looking at an id
     *
     * @param id of email to look for
     * @return EmailBean a single email and its content
     * @throws java.sql.SQLException
     */
    @Override
    public EmailBean findById(int id) throws SQLException {
        EmailBean emailBean = new EmailBean();
        String newSelectQuery = this.selectQuery + " where EmailId = ?";
        try ( Connection connection = DriverManager.getConnection(mySqlUrl, mailConfigBean.getMysqlUser(), mailConfigBean.getMysqlPassword()); // Using a prepared statement to handle the conversion
                  PreparedStatement stmt = connection.prepareStatement(newSelectQuery);) {
            stmt.setInt(1, id);
            try ( ResultSet resultSet = stmt.executeQuery()) {
                if (resultSet.next()) {
                    emailBean = createEmailBeanData(resultSet);
                }
            }
        }
        LOG.info("Email id found: " + id);
        return emailBean;
    }

    /**
     * This method finds all the fields that a user would like to see from a
     * sent or received email.
     *
     * @return List containing all the rows of emails
     * @throws SQLException
     */
    @Override
    public List<EmailBean> findAll() throws SQLException {
        //list to return
        List<EmailBean> rows = new ArrayList<>();
        //create connection 
        try ( Connection connection = DriverManager.getConnection(this.mySqlUrl, mailConfigBean.getMysqlUser(), mailConfigBean.getMysqlPassword()); // Using a prepared statement to handle the conversion
                //sql injection safety
                  PreparedStatement ps = connection.prepareStatement(this.selectQuery); // Retrieve generated primary key value and assign to bean
                  ResultSet resultSet = ps.executeQuery()) {
            while (resultSet.next()) {
                rows.add(createEmailBeanData(resultSet));
            }
        }
        LOG.info("# of total email records found : " + rows.size());
        return rows;
    }

    /**
     * This method searches for a string inside an email, text, html, subject It
     * includes the info from the email too which can be sent or received.
     *
     * @param search string to look for
     * @return List returning all emails with that string
     * @throws SQLException
     */
    @Override
    public List<EmailBean> findFields(String search) throws SQLException {
        //list to return
        List<EmailBean> rows = new ArrayList<>();

        String newSelectQuery = this.selectQuery + " where FromAddress like ?"
                + " or Subject like ? or TextMessage like ? or HtmlMessage like ?";
        //have to add this so that the the fields are searched properly in the sql
        String searchForLike = "%" + search + "%";

        try ( Connection connection = DriverManager.getConnection(this.mySqlUrl, mailConfigBean.getMysqlUser(), mailConfigBean.getMysqlPassword()); // Using a prepared statement to handle the conversion
                // sql Injection safety
                  PreparedStatement pStatement = connection.prepareStatement(newSelectQuery);) {
            // must search the same thing in diffferent fields
            pStatement.setString(1, searchForLike);
            pStatement.setString(2, searchForLike);
            pStatement.setString(3, searchForLike);
            pStatement.setString(4, searchForLike);
            // A new try-with-resources block for creating the ResultSet object begins
            try ( ResultSet resultSet = pStatement.executeQuery()) {
                while (resultSet.next()) {
                    rows.add(createEmailBeanData(resultSet));
                }
            }
            LOG.info("# of email records found: " + rows.size() + "searching with string: " + search);
            return rows;
        }
    }

    /**
     * This method finds all the emails in a folder it includes info of the
     * emails too.I use the folder name as input and not id since i want to be
     * able to display the folder name
     *
     * @param folderid
     * @return List containing all the emails in a specific folder
     * @throws SQLException
     */
    @Override
    public List<EmailBean> findFolder(int folderid) throws SQLException {
        //list to return
        List<EmailBean> rows = new ArrayList<>();

        String newSelectQuery = this.selectQuery
                + " left outer join Folder using(FolderId)"
                + " where FolderId = ?";

        try ( Connection connection = DriverManager.getConnection(this.mySqlUrl, mailConfigBean.getMysqlUser(), mailConfigBean.getMysqlPassword()); // Using a prepared statement to handle the conversion
                // sql Injection safety
                  PreparedStatement pStatement = connection.prepareStatement(newSelectQuery);) {
            // setting folderName
            pStatement.setInt(1, folderid);
            // A new try-with-resources block for creating the ResultSet object begins
            try ( ResultSet resultSet = pStatement.executeQuery()) {
                while (resultSet.next()) {
                    rows.add(createEmailBeanData(resultSet));

                }
            }
            LOG.info("# of email records found: " + rows.size() + ", Inside this folder");
            return rows;
        }

    }

    /**
     * This method will find the regular attachments of a table while looking at
     * the userid it will then add it to the emailBean
     *
     * @param emailBean to add attachments to
     * @throws SQLException
     */
    private void findRegularAttachments(EmailBean emailBean) throws SQLException {
        int result = 0;

        String newSelectQuery = "select FileName, ContentId, Attachment from Attachment where EmailId = ?"
                + " and (ContentId is null or ContentId = '')";

        //creating connection
        try ( Connection connection = DriverManager.getConnection(this.mySqlUrl, mailConfigBean.getMysqlUser(), mailConfigBean.getMysqlPassword()); // Using a prepared statement to handle the conversion
                // sql Injection safety
                  PreparedStatement pStatement = connection.prepareStatement(newSelectQuery);) {
            // setting folderName
            pStatement.setInt(1, emailBean.getId());
            // A new try-with-resources block for creating the ResultSet object begins
            try ( ResultSet resultSet = pStatement.executeQuery()) {
                while (resultSet.next()) {
                    byte[] attachment = resultSet.getBytes("Attachment");
                    String name = resultSet.getString("FileName");
                    emailBean.email.attachment(EmailAttachment.with().content(attachment).name(name));
                    result++;
                }

            }
        }
        LOG.info("# of regular attachments added to bean: " + result);
    }

    /**
     *   * This method will find the embedded attachments of a table while
     * looking at the userid it will then add it to the emailBean
     *
     * @param emailBean to add attachments to
     * @throws SQLException
     */
    private void findEmbeddedAttachments(EmailBean emailBean) throws SQLException {
        int result = 0;
        String newSelectQuery = "select FileName, ContentId, Attachment from Attachment where emailId = ?"
                + " and (ContentId is not null or ContentId != '')";
        //creating connection
        try ( Connection connection = DriverManager.getConnection(this.mySqlUrl, mailConfigBean.getMysqlUser(), mailConfigBean.getMysqlPassword()); // Using a prepared statement to handle the conversion
                // sql Injection safety
                  PreparedStatement pStatement = connection.prepareStatement(newSelectQuery);) {
            // setting folderName
            pStatement.setInt(1, emailBean.getId());
            // A new try-with-resources block for creating the ResultSet object begins
            try ( ResultSet resultSet = pStatement.executeQuery()) {
                while (resultSet.next()) {
                    byte[] attachment = resultSet.getBytes("Attachment");
                    String name = resultSet.getString("FileName");
                    String contentId = resultSet.getString("ContentId");
                    emailBean.email.embeddedAttachment(EmailAttachment.with().content(attachment).name(name).contentId(contentId));
                    result++;
                }

            }
        }
        LOG.info("# of embedded attachments added to bean: " + result);
    }

    /**
     * this will get all the folders in a user's database it will create a
     * observableList of emailBean which will contain all the folderBeans
     *
     * @return List containing all the folders for a user
     * @throws SQLException
     */
    @Override
    public List<FolderBean> findAllFolders() throws SQLException {
        //list to return
        List<FolderBean> rows = new ArrayList<>();

        String selectSpecificQuery = "select folderid, foldername from folder";
        try ( Connection connection = DriverManager.getConnection(this.mySqlUrl, mailConfigBean.getMysqlUser(), mailConfigBean.getMysqlPassword()); // Using a prepared statement to handle the conversion
                // sql Injection safety
                  PreparedStatement pStatement = connection.prepareStatement(selectSpecificQuery);) {
            // A new try-with-resources block for creating the ResultSet object begins
            try ( ResultSet resultSet = pStatement.executeQuery()) {
                while (resultSet.next()) {
                    rows.add(new FolderBean(resultSet.getInt("folderid"), resultSet.getString("folderName")));
                }
            }
            LOG.info("# of rows found: " + rows.size());
            return rows;
        }
    }

    /**
     * this creates the email bean data from the result set It also adds the
     * fields of the email inside the bean
     *
     * @param rs
     * @return EmailBean with all info for an email
     * @throws SQLException
     */
    private EmailBean createEmailBeanData(ResultSet rs) throws SQLException {
        EmailBean emailBean = new EmailBean();
        emailBean.setId(rs.getInt("EmailId"));
        emailBean.setFolderKey(rs.getInt("FolderId"));

        //if receiveddate is null ,then this email has not been received and should not have a bcc field
        if (Objects.isNull(rs.getTimestamp("ReceivedDate"))) {
            emailBean.setReceivedDate(null);
            addEmailTypeBeanData(emailBean, rs.getInt("EmailId"), true);
        } else {
            emailBean.setReceivedDate(rs.getTimestamp("ReceivedDate").toLocalDateTime());
            addEmailTypeBeanData(emailBean, rs.getInt("EmailId"), false);
        }

        //only add a sent date if the email has been sent or else null pointer exception
        if (Objects.nonNull(rs.getTimestamp("SendDate"))) {
            //changing the timestamp into a date
            Timestamp stamp = rs.getTimestamp("SendDate");
            Date date = new Date(stamp.getTime());
            
            //setting the sent date value
            emailBean.email.sentDate(date);
        }

        findRegularAttachments(emailBean);
        findEmbeddedAttachments(emailBean);
        emailBean.email.subject(rs.getString("Subject"))
                .textMessage(rs.getString("TextMessage"))
                .htmlMessage(rs.getString("HtmlMessage"))
                .from(rs.getString("FromAddress"));
        return emailBean;
    }

    /**
     * This method allows the email to add the to, cc, bcc fields. The user
     * should be able to tell how they received their email
     *
     * @param emailBean to add to
     * @param id of the email to add to
     * @param received wether the email is received or not will determine if we
     * should add the bcc
     * @return Email
     * @throws SQLException
     */
    private void addEmailTypeBeanData(EmailBean emailBean, int id, boolean received) throws SQLException {
        String newSelectQuery = "select distinct EmailId, EmailAddress, Category from EmailToAddress"
                + " inner join Address on EmailAddressId = RecipientEmailAddressId"
                + " where EmailId=?";
        //creating connecton
        try ( Connection connection = DriverManager.getConnection(this.mySqlUrl, mailConfigBean.getMysqlUser(), mailConfigBean.getMysqlPassword()); // Using a prepared statement to handle the conversion
                // sql Injection safety
                  PreparedStatement pStatement = connection.prepareStatement(newSelectQuery);) {
            //setting
            pStatement.setInt(1, id);
            // A new try-with-resources block for creating the ResultSet object begins
            try ( ResultSet resultSet = pStatement.executeQuery()) {
                while (resultSet.next()) {
                    //checks the category and adds it to the field 
                    if (resultSet.getString("Category").contentEquals("TO")) {
                        emailBean.email.to(resultSet.getString("EmailAddress"));
                        LOG.info("added field To");
                    } else if (resultSet.getString("Category").contentEquals("CC")) {
                        emailBean.email.cc(resultSet.getString("EmailAddress"));
                        LOG.info("added field CC");
                    } else if (resultSet.getString("Category").contentEquals("BCC") && (received)) {
                        //if the email is recived, it will not be added to the email since a recivedemail does not have a bcc
                        emailBean.email.bcc(resultSet.getString("EmailAddress"));
                        LOG.info("added field BCC");
                    }
                }
            }
        }
    }

    /* UPDATE ***********************************************************************************************************************/
    /**
     * This will update the folder. In other words, handles drag and dropping
     * files to a different folder. Also makes sure the email is not put into or
     * taken from the draft folder
     *
     * @param emailBean email to update
     * @return int result the amount of records updated
     * @throws SQLException
     */
    @Override
    public int updateFolder(EmailBean emailBean) throws SQLException {
        int result;
        String updateQuery = "update Email"
                + " set FolderId = ? where EmailId = ?";
        //creating connection
        try ( Connection connection = DriverManager.getConnection(this.mySqlUrl, mailConfigBean.getMysqlUser(), mailConfigBean.getMysqlPassword()); // Using a prepared statement to handle the conversion
                  PreparedStatement pStatement = connection.prepareStatement(updateQuery);) {
            fillPreparedStatementForFolderUpdate(pStatement, emailBean);
            result = pStatement.executeUpdate();
        }
        LOG.info("# of folder records updated: " + result);
        return result;
    }

    /**
     * updates the content of a draft including its subject, text message , html
     * message
     *
     * @param emailBean
     * @return int the amount of records updated
     * @throws SQLException
     */
    @Override
    public int updateDraft(EmailBean emailBean) throws SQLException {
        int result = -1;
        String updateQuery = "update Email"
                + " set Subject = ?, TextMessage = ?, HtmlMessage = ? where EmailId = ?";
        LOG.debug("the subject is : " + emailBean.email.subject());
        //creating connection
        try ( Connection connection = DriverManager.getConnection(this.mySqlUrl, mailConfigBean.getMysqlUser(), mailConfigBean.getMysqlPassword()); // Using a prepared statement to handle the conversion
                  PreparedStatement pStatement = connection.prepareStatement(updateQuery);) {
            fillPreparedStatementForUpdateDraft(pStatement, emailBean);
            result = pStatement.executeUpdate();
        }
        //delete the current to, cc, bcc
        deleteEmailToAddress(emailBean.getId());

        //add new to, cc, bcc
        addEmailTypeBeanData(emailBean, emailBean.getId(), false);
        createEmailToAddressTO(emailBean);
        createEmailToAddressCC(emailBean);
        createEmailToAddressBCC(emailBean);
        //delete current attachment
        deleteAttachment(emailBean.getId());
        //add new attachment
        createAttachment(emailBean);

        LOG.info("# of records updated: " + result);
        return result;

    }

    /**
     * this will rename the folder only if it is not called inbox, sent ,draft
     *
     * @param oldFolderName
     * @param newFolderName
     * @return the amount of records updated
     * @throws SQLException
     * @throws CannotRenameFolderException
     * @throws com.obied.exception.FolderAlreadyExistsException
     */
    @Override
    public int renameFolder(String oldFolderName, String newFolderName) throws SQLException, CannotRenameFolderException, FolderAlreadyExistsException {
        int result;
        //check folder can be update
        if (oldFolderName.contentEquals("Inbox") || oldFolderName.contentEquals("Draft") || oldFolderName.contentEquals("Sent")) {
            throw new CannotRenameFolderException("cannot update the folder: " + oldFolderName);
        }
        String updateQuery = "update folder set folderName=? where folderName=?";
        updateEmailFolderIds(newFolderName);
        //creating connection
        try ( Connection connection = DriverManager.getConnection(this.mySqlUrl, mailConfigBean.getMysqlUser(), mailConfigBean.getMysqlPassword()); // Using a prepared statement to handle the conversion
                  PreparedStatement pStatement = connection.prepareStatement(updateQuery);) {
            pStatement.setString(1, newFolderName);
            pStatement.setString(2, oldFolderName);
            result = pStatement.executeUpdate();
        } catch (SQLIntegrityConstraintViolationException e) {
            throw new FolderAlreadyExistsException("The folder already exists: " + newFolderName);
        }
        LOG.info("# of records updated: " + result);
        return result;
    }

    /**
     * updating the folder name
     *
     * @param folderName
     * @throws SQLException
     */
    private void updateEmailFolderIds(String folderName) throws SQLException {
        String updateQuery = "update from Email set folderId = (select FolderId from Folder where FolderName = ?)";
        //creating connection
        try ( Connection connection = DriverManager.getConnection(this.mySqlUrl, mailConfigBean.getMysqlUser(), mailConfigBean.getMysqlPassword()); // Using a prepared statement to handle the conversion
                  PreparedStatement pStatement = connection.prepareStatement(updateQuery);) {
            pStatement.setString(1, folderName);
        }
    }

    /**
     * This fills the prepared statement for the update folder
     *
     * @param ps
     * @param emailBean
     * @throws SQLException
     */
    private void fillPreparedStatementForFolderUpdate(PreparedStatement ps, EmailBean emailBean) throws SQLException {
        ps.setInt(1, emailBean.getFolderKey());
        ps.setInt(2, emailBean.getId());
    }

    /**
     * This fills the prepared statement for the update Draft
     *
     * @param ps
     * @param emailBean
     * @throws SQLException
     */
    private void fillPreparedStatementForUpdateDraft(PreparedStatement ps, EmailBean emailBean) throws SQLException {
        LOG.debug("the subject is : " + emailBean.email.subject());
        ps.setString(1, emailBean.email.subject());

        //get all messages and if its a html then add it to the html message
        for (EmailMessage message : emailBean.email.messages()) {
            if (message.getMimeType().equals("text/html")) {
                ps.setString(3, message.getContent());
            } else {
                ps.setString(2, message.getContent());
            }
        }
        ps.setInt(4, emailBean.getId());
    }


    /* DELETE *******************************************************************************************************************/
    /**
     * calls helper methods to delete an email
     *
     * @param id
     * @return int the amount of records deleted
     * @throws SQLException
     */
    @Override
    public int delete(int id) throws SQLException {
        deleteEmailToAddress(id);
        deleteAttachment(id);
        return deleteEmail(id); //if delete emails works then the other two work
    }

    /**
     * deletes a folder from its folderName
     *
     * @param folderName
     * @return int amount of records deleted
     * @throws SQLException
     * @throws com.obied.exception.InvalidDeleteFolderException
     */
    @Override
    public int deleteFolder(String folderName) throws SQLException, InvalidDeleteFolderException {
        int result;
        //check if user deleting the wrong email
        if (folderName.contentEquals("Inbox") || folderName.contentEquals("Sent") || folderName.contentEquals("Draft")) {
            throw new InvalidDeleteFolderException("CANNOT DELETE INBOX, SENT , DRAFT");
        }
        String deleteQuery = "delete from Folder where FolderName = ? ";

        //delete the emails first to respect foreign keys
        deleteAllEmailsInFolder(folderName);
        //creating connection
        try ( Connection connection = DriverManager.getConnection(this.mySqlUrl, mailConfigBean.getMysqlUser(), mailConfigBean.getMysqlPassword()); // Using a prepared statement to handle the conversion
                  PreparedStatement pStatement = connection.prepareStatement(deleteQuery);) {
            pStatement.setString(1, folderName);
            result = pStatement.executeUpdate();
        }
        LOG.info("# of folder records deleted: " + result);
        return result;
    }

    /**
     * delete records from the emailToAddress table from an emailId
     *
     * @param id
     * @throws SQLException
     */
    private void deleteEmailToAddress(int id) throws SQLException {
        int result;

        String deleteQuery = "delete from EmailToAddress where EmailId = ?";
        //creating connection
        try ( Connection connection = DriverManager.getConnection(this.mySqlUrl, mailConfigBean.getMysqlUser(), mailConfigBean.getMysqlPassword()); // Using a prepared statement to handle the conversion
                  PreparedStatement pStatement = connection.prepareStatement(deleteQuery);) {
            pStatement.setInt(1, id);
            result = pStatement.executeUpdate();
        }
        LOG.info("# of EmailToAddress records deleted: " + result);
    }

    /**
     * delete records from the emailToAddress table from an emailId
     *
     * @param id
     * @throws SQLException
     */
    private void deleteAttachment(int id) throws SQLException {
        int result;
        String deleteQuery = "delete from Attachment where EmailId = ?";
        //creating connection
        try ( Connection connection = DriverManager.getConnection(this.mySqlUrl, mailConfigBean.getMysqlUser(), mailConfigBean.getMysqlPassword()); // Using a prepared statement to handle the conversion
                  PreparedStatement pStatement = connection.prepareStatement(deleteQuery);) {
            pStatement.setInt(1, id);
            result = pStatement.executeUpdate();
        }
        LOG.info("# of attachment records deleted: " + result);
    }

    /**
     * deletes an email record from an id
     *
     * @param id
     * @return
     * @throws SQLException
     */
    private int deleteEmail(int id) throws SQLException {
        int result;
        String deleteQuery = "delete from Email where EmailId = ?";
        //creating connection
        try ( Connection connection = DriverManager.getConnection(this.mySqlUrl, mailConfigBean.getMysqlUser(), mailConfigBean.getMysqlPassword()); // Using a prepared statement to handle the conversion
                  PreparedStatement pStatement = connection.prepareStatement(deleteQuery);) {
            pStatement.setInt(1, id);
            result = pStatement.executeUpdate();
        }
        LOG.info("# of email records deleted: " + result);
        return result;
    }

    /**
     * delete all email records inside a folder
     *
     * @param folderName
     * @throws SQLException
     */
    private void deleteAllEmailsInFolder(String folderName) throws SQLException {
        int result;
        //must delete bridging tables first
        deleteBridgingTables(getIds(folderName));
        String deleteQuery = "delete from Email where FolderId = (select FolderId from Folder where FolderName = ?)";

        //creating connection
        try ( Connection connection = DriverManager.getConnection(this.mySqlUrl, mailConfigBean.getMysqlUser(), mailConfigBean.getMysqlPassword()); // Using a prepared statement to handle the conversion
                  PreparedStatement pStatement = connection.prepareStatement(deleteQuery);) {
            pStatement.setString(1, folderName);
            result = pStatement.executeUpdate();
        }
        LOG.info("# of email records deleted: " + result);
    }

    /**
     * this method gets all the ids of the emails to be deleted form a folder
     *
     * @param folderName
     * @return
     * @throws SQLException
     */
    private ArrayList<Integer> getIds(String folderName) throws SQLException {
        int result;
        String deleteQuery = "select EmailId from Email where FolderId = (select FolderId from Folder where FolderName = ?)";

        ArrayList<Integer> ids = new ArrayList<>();
        //creating connection
        try ( Connection connection = DriverManager.getConnection(this.mySqlUrl, mailConfigBean.getMysqlUser(), mailConfigBean.getMysqlPassword()); // Using a prepared statement to handle the conversion
                  PreparedStatement pStatement = connection.prepareStatement(deleteQuery);) {
            pStatement.setString(1, folderName);
            // A new try-with-resources block for creating the ResultSet object begins
            try ( ResultSet resultSet = pStatement.executeQuery()) {
                while (resultSet.next()) {
                    //add the ids
                    ids.add(resultSet.getInt("EmailId"));
                }
            }
        }
        return ids;
    }

    /**
     * deletes the bridging tables record having the ids of the folder
     *
     * @param emailIds
     * @throws SQLException
     */
    private void deleteBridgingTables(ArrayList<Integer> emailIds) throws SQLException {
        for (Integer emailId : emailIds) {
            deleteAttachment(emailId);
            deleteEmailToAddress(emailId);
        }
    }

    @Override
    public int findFolderId(String folderName) throws SQLException {
        int id = -1;

        String newSelectQuery = "select folderid from folder where foldername = ?";

        try ( Connection connection = DriverManager.getConnection(this.mySqlUrl, mailConfigBean.getMysqlUser(), mailConfigBean.getMysqlPassword()); // Using a prepared statement to handle the conversion
                // sql Injection safety
                  PreparedStatement pStatement = connection.prepareStatement(newSelectQuery);) {
            // setting folderName
            pStatement.setString(1, folderName);
            ResultSet resultSet = pStatement.executeQuery();
            if (resultSet.next()) {
                id = resultSet.getInt("folderid");
            }
            LOG.info("folder id found: " + id);
            return id;
        }
    }

    /**
     * this will save a blob file to the root folder of the project
     *
     * @param emailId
     * @throws SQLException
     */
    public void saveBlobToDisk(int emailId) throws SQLException {
        LOG.debug("inside blob save");
        String query = "select filename, attachment from attachment where emailid = ?";
        try ( Connection connection = DriverManager.getConnection(this.mySqlUrl, mailConfigBean.getMysqlUser(), mailConfigBean.getMysqlPassword()); // Using a prepared statement to handle the conversion
                // sql Injection safety
                  PreparedStatement pStatement = connection.prepareStatement(query);) {

            // setting folderName
            pStatement.setInt(1, emailId);
            ResultSet resultSet = pStatement.executeQuery();
            while (resultSet.next()) {
                String fileName = resultSet.getString("filename");
                LOG.debug("the filename is: " + fileName);
                File image = new File(fileName);
                FileOutputStream fos;
                fos = new FileOutputStream(image);
                byte[] buffer = new byte[1];
                InputStream is = resultSet.getBinaryStream("attachment");
                while (is.read(buffer) > 0) {
                    fos.write(buffer);
                }
                fos.close();
            }
        } catch (FileNotFoundException ex) {
            LOG.debug("FILENOTFOUNDEXCEPTION THROWN");
        } catch (IOException ex) {
            LOG.debug("IOEXCEPTION THROWN");
        } catch (NullPointerException ex) {
            // there is no email to save
        }

    }

}
