package com.obied.jag_project;

import com.obied.beans.MailConfigBean;
import com.obied.beans.propertiesmanager.PropertiesManager;
import com.obied.controllers.RootLayoutController;
import com.obied.controllers.config.EmailFXMailConfigFormController;
import java.io.IOException;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.Locale;
import java.util.ResourceBundle;
import javafx.application.Application;
import static javafx.application.Application.launch;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.layout.BorderPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * In this class we are creating an email application. first it must get the
 * information of the database and user through a mailconfigform and after
 * getting the information or loading the already set information from a file
 * thanks to the propertiesmanager class, it will display the email app through
 * the root layout
 *
 * 
 * 
 * @author Obied
 */
public class MainAppFX extends Application {

    private final static Logger LOG = LoggerFactory.getLogger(MainAppFX.class);

    private Stage primaryStage;
    private Parent rootLayout;
    private Locale currentLocale;

    @Override
    public void start(Stage primaryStage) throws Exception {
        this.primaryStage = primaryStage;

        //creates default values that will be changed later
        PropertiesManager propertiesManager = new PropertiesManager();
        MailConfigBean mailConfig = new MailConfigBean();

        //this will either select the MailConfigFormLayout or the RootLayout
        selectRootLayout(propertiesManager, mailConfig);

        //exit program if the user exits the mailconfigform application
        checkRootIsNull();

        //this will create the rootLayout
        RootLayoutSetup();
    }

    /**
     * this will select the layout needed at the beginning. if the properties
     * are not empty then it will load the config form or else it will load the
     * root layout in the beginning if the properties were already set. It also
     * loads the root layout after the mail config form is properly set inside
     * the start method
     *
     * @param propertiesManager
     * @param mailConfig
     * @throws IOException
     * @throws SQLException
     * @throws ParseException
     */
    private void selectRootLayout(PropertiesManager propertiesManager, MailConfigBean mailConfig) throws IOException, SQLException, ParseException {
        //get the language of the device
        currentLocale = Locale.getDefault();
        LOG.debug("Locale = " + currentLocale);
        //if a mailConfigBean property or more is empty, it will call the mailconfiglayout to set the properties
        boolean isValidProperties = propertiesManager.loadTextProperties(mailConfig, "", "mailconfig");
        if (!isValidProperties) {
            initMailConfigLayout(propertiesManager, mailConfig);
        }

        //if it has valid values, it will call the rootLayout
        //must call it again since the properties might have changed
        isValidProperties = propertiesManager.loadTextProperties(mailConfig, "", "mailconfig");
        if (isValidProperties) {
            initRootLayout(mailConfig);
        }

    }

    /**
     * this will load the root layout and controller. it will be bale to set the
     * emailDao since it gets the values from the mailconfigBean.
     *
     * the creation of the layouts are made in the setemaildao not the
     * initialization of the controller.
     *
     * @param mailConfigBean
     * @throws IOException
     * @throws SQLException
     * @throws ParseException
     */
    public void initRootLayout(MailConfigBean mailConfigBean) throws IOException, SQLException, ParseException {

        FXMLLoader loader = new FXMLLoader();
        loader.setResources(ResourceBundle.getBundle("MessagesBundle")); //, currentLocale));
        loader.setLocation(MainAppFX.class.
                getResource("/fxml/RootLayout.fxml"));
        rootLayout = (BorderPane) loader.load();
        RootLayoutController rootLayoutController = loader.getController();
        rootLayoutController.initializeFields(mailConfigBean);

    }

    /**
     * Load the mail config form layout and controller Uses the properties
     * manager and mailconfig to set the already initialized mailConifgBean
     * values inside the form
     *
     * @param propertiesManager
     * @param mailConfig
     * @throws IOException
     */
    private void initMailConfigLayout(PropertiesManager propertiesManager, MailConfigBean mailConfig) throws IOException {
        FXMLLoader configLoader = new FXMLLoader();
        configLoader.setResources(ResourceBundle.getBundle("MessagesBundle", currentLocale));
        configLoader.setLocation(MainAppFX.class
                .getResource("/fxml/EmailFXConfigFormLayout.fxml"));
        Parent settingsRoot = configLoader.load();
        EmailFXMailConfigFormController config = configLoader.getController();

        //setup the properties file
        config.setUpProperties(propertiesManager, mailConfig);
        mailConfigSetup(settingsRoot);
    }

    /**
     * this method will set the stage for the root layout
     */
    private void RootLayoutSetup() {
        //sets up the primary stage
        this.primaryStage.setTitle(ResourceBundle.getBundle("MessagesBundle", currentLocale).getString("title"));
        // Set the application icon using getResourceAsStream.
        this.primaryStage.getIcons().add(new Image(MainAppFX.class.getResourceAsStream("/images/email.png")));
        Scene scene = new Scene(rootLayout);
        primaryStage.setScene(scene);
        this.primaryStage.sizeToScene();
        LOG.info("Root Layout Stage Showed");
        primaryStage.show();

    }

    /**
     * this method will set the stage for the mail config form
     *
     * @param settingsRoot
     */
    private void mailConfigSetup(Parent settingsRoot) {
        //create the stage to show application
        Stage configStage = new Stage();
        configStage.setScene(new Scene(settingsRoot));
        configStage.setTitle(ResourceBundle.getBundle("MessagesBundle").getString("titleConfig"));
        // Set the application icon using getResourceAsStream.
        configStage.getIcons().add(new Image(MainAppFX.class.getResourceAsStream("/images/email.png")));
        configStage.setResizable(false);
        configStage.initModality(Modality.WINDOW_MODAL);
        LOG.info("Mail Config Stage Show and Wait");

        configStage.showAndWait();
    }

    /**
     * this method checks if the root is null since when a user exits the
     * mailConfigForm, it will be null and at that moment we want to end the
     * application
     */
    private void checkRootIsNull() {
        if (rootLayout == null) {
            LOG.debug("Root was null");
            System.exit(0);
        }
    }

    /**
     * Where it all begins
     *
     * @param args
     */
    public static void main(String[] args) {
        launch(args);
        System.exit(0);
    }
}
