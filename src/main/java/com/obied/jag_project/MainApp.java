package com.obied.jag_project;

/**
 * calls MainAppFX
 * @author Obied
 */
public class MainApp {
    
     public static void main(String[] args) {
        MainAppFX.main(args);
        System.exit(0);
    }
    
}
