/**
 * Here is your documentation on Jodd
 * https://jodd.org/email/index.html
 */
package com.obied.business;

import com.obied.beans.MailConfigBean;
import com.obied.exception.InvalidEmailFormatException;
import com.obied.exception.NoEmailFoundException;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.List;
import java.util.Objects;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.mail.Flags;

import jodd.mail.EmailFilter;
import jodd.mail.Email;
import jodd.mail.EmailAttachment;
import jodd.mail.ImapServer;
import jodd.mail.MailServer;
import jodd.mail.RFC2822AddressParser;
import jodd.mail.ReceiveMailSession;
import jodd.mail.ReceivedEmail;
import jodd.mail.SendMailSession;
import jodd.mail.SmtpServer;

/**
 * This is a class that will send emails using to, cc and bcc. The emails can
 * contain embedded image and an attachment. This class can also receive emails
 * including attachments
 *
 * @author Obied
 * @version 1.0
 *
 */
public class SendAndReceive {

    private final static Logger LOG = LoggerFactory.getLogger(SendAndReceive.class);
    private final MailConfigBean sender;
    private final int secondsToSleep = 3;

    public SendAndReceive(MailConfigBean sender) {
        this.sender = sender;
    }

    /**
     * send email using Jodd.This validates the sender's email and sends the
     * message including attachments or images to someone or to many people
     * using to,cc,bcc.Jodd knows about GMail so no need to include port
     * information.
     *
     * @param toList List of people to send the email
     * @param ccList List of people to send a carbon copy
     * @param bccList List of people to send a blind carbon copy
     * @param subject of email
     * @param textMsg text of email
     * @param htmlMsg html of email
     * @param regularAttachments attachments in email
     * @param embeddedAttachments embedded attachments in email
     * @return Email with all its content filled or null if there that is not
     * expected such as
     * @throws java.io.FileNotFoundException
     * @throws com.obied.exception.InvalidEmailFormatException
     * @throws com.obied.exception.NoEmailFoundException
     *
     */
    public Email send(List<String> toList, List<String> ccList, List<String> bccList, String subject, String textMsg, String htmlMsg, List<File> regularAttachments, List<File> embeddedAttachments) throws FileNotFoundException, InvalidEmailFormatException, NoEmailFoundException {
        // the sender's email address format is invalid
        if (!checkEmail(this.sender.getEmailAddress())) {
            LOG.info("Unable to send email because the sender's address is invalid");
            throw new InvalidEmailFormatException("The sender's email address is invalid");
        }

        //the message is being sent to no one
        if (toList.isEmpty() && ccList.isEmpty() && bccList.isEmpty()) {
            LOG.info("Unable to send email because the message is being sent to no one");
            throw new NoEmailFoundException("there is no receiver for this email");
        }

        //the format must be valid for all emails
        if (checkEmail(toList) && checkEmail(ccList) && checkEmail(bccList)) {
            //create SMTPserver 
            SmtpServer smtpServer = createSmtpServer();

            //create email
            Email email = createEmail(subject, textMsg, htmlMsg);

            //adding the attachments
            addAttachments(email, regularAttachments, false);
            addAttachments(email, embeddedAttachments, true);

            //sending the emails
            sendEmailMethods(email, toList, "to");
            sendEmailMethods(email, ccList, "cc");
            sendEmailMethods(email, bccList, "bcc");
            sendEmail(smtpServer, email);
            
            return email;
        }
        //will never reach here
        return null;
    }

    /**
     * Receive Email using Jodd using an ImapServer.
     *
     * @param receiver user who is receiving an email
     * @return ReceivedEmail[] containing all the emils said user has received
     * @throws com.obied.exception.InvalidEmailFormatException
     */
    public ReceivedEmail[] receiveEmail(MailConfigBean receiver) throws InvalidEmailFormatException {
        if (checkEmail(receiver.getEmailAddress())) {
            // Create am IMAP server object
            ImapServer imapServer = createImapServer(receiver);
            //create a session 
            try ( ReceiveMailSession session = imapServer.createSession()) {
                session.open();
                ReceivedEmail[] emails = session.receiveEmailAndMarkSeen(EmailFilter.filter().flag(Flags.Flag.SEEN, false));
                return emails;
            }
        } else {
            LOG.info("Unable to send email because either send or recieve addresses are invalid");
            throw new InvalidEmailFormatException("the receiver's email address is invalid");
        }
    }

    /**
     * Use the RFC2822AddressParser to validate that the email string could be a
     * valid address
     *
     * @param address
     * @return true is OK, false if not
     */
    private boolean checkEmail(String address) {
        return RFC2822AddressParser.STRICT.parseToEmailAddress(address) != null;
    }

    /**
     * Use the RFC2822AddressParser to validate that the email string could be a
     * valid address
     *
     * @param emails
     * @return true is OK, false if not
     */
    private boolean checkEmail(List<String> emails) throws InvalidEmailFormatException {
        for (String addresses : emails) {
            if (!checkEmail(addresses)) {
                LOG.info("Unable to send email because format is invalid");
                throw new InvalidEmailFormatException("invalid email format for " + addresses);
            }
        }
        return true;
    }

    /**
     * this creates the smtp server
     *
     * @return SmtpServer
     */
    private SmtpServer createSmtpServer() {
        // Create am SMTP server object
        SmtpServer smtpServer = MailServer.create()
                .ssl(true)
                .host(this.sender.getSmtpURL())
                .auth(this.sender.getEmailAddress(), this.sender.getMailPassword())
                //.debugMode(true)
                .buildSmtpMailServer();
        return smtpServer;
    }

    /**
     * this creates an imap server
     *
     * @param receiver to get the authentication info
     * @return
     */
    private ImapServer createImapServer(MailConfigBean receiver) {
        // Create am IMAP server object
        ImapServer imapServer = MailServer.create()
                .host(receiver.getImapURL())
                .ssl(true)
                .auth(receiver.getEmailAddress(), receiver.getMailPassword())
                //.debugMode(true)
                .buildImapMailServer();
        return imapServer;
    }

    /**
     * this creates the email and adds fields to it
     *
     * @param subject
     * @param textMsg
     * @param htmlMsg
     * @return Email with the content
     */
    private Email createEmail(String subject, String textMsg, String htmlMsg) {
        //creating the email object, some methods of the object are used in all three : to, cc, bcc
        Email email = Email.create().from(this.sender.getEmailAddress())
                .subject(subject)
                .textMessage(textMsg)
                .htmlMessage(htmlMsg);
        return email;
    }

    /**
     * This adds the attachments to the email depending on wether they are
     * embedded or not.
     *
     * @param email
     * @param attachments
     * @param isEmbedded
     * @throws FileNotFoundException
     */
    private void addAttachments(Email email, List<File> attachments, boolean isEmbedded) throws FileNotFoundException {
        //make sure list is not empty
        if (Objects.nonNull(attachments) && !attachments.isEmpty()) {
            //go through the list
            for (File file : attachments) {
                //check if the attachment to add is embedded
                if (isEmbedded) {
                    if (file.exists()) {
                        //embedded attachments
                        email.embeddedAttachment(EmailAttachment.with().name(file.getName()).content(file).contentId(file.toString()));
                    } else {
                        LOG.info("Invalid embedded attachments");
                        throw new FileNotFoundException("no valid embedded attachments dound ");
                    }
                } else { // if the file is not embedded (regular attachments)
                    if (file.exists()) {
                        // regular attachments
                        email.attachment(EmailAttachment.with().content(file).name(file.getName()));
                    } else {
                        LOG.info("Invalid attachment");
                        throw new FileNotFoundException("no valid regular attachments found");
                    }
                }
            }
        }
    }

    /**
     * This sends email depending on their type (to, cc, bcc)
     *
     * @param email
     * @param list
     * @param type
     */
    private void sendEmailMethods(Email email, List<String> list, String type) {
        for (String emailReceiver : list) {
            //if email sent by to
            if (type.contentEquals("to")) {
                email.to(emailReceiver);
            }
            //if email sent by cc
            if (type.contentEquals("cc")) {
                email.cc(emailReceiver);
            }
            //if email sent by bcc
            if (type.contentEquals("bcc")) {
                email.bcc(emailReceiver);
            }
        }
    }

    /**
     * Like a file we open the session, send the email and close the session
     *
     * @param smtpServer server that we are using
     * @param email email to be sent
     */
    private void sendEmail(SmtpServer smtpServer, Email email) {
        try ( // A session is the object responsible for communicating with the server
                 SendMailSession session = smtpServer.createSession()) {
            // Like a file we open the session, send the message and close the
            // session
            session.open();
            session.sendMail(email);
            LOG.info("Email sent");
        }
    }

}
