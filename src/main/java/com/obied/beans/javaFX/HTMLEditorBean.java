package com.obied.beans.javaFX;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 * This beans holds the value for the message in the html
 *
 * @author Obied
 */
public class HTMLEditorBean {

    private StringProperty htmlMessageField;

    /**
     * initializes the value
     * @param message
     */
    public HTMLEditorBean(String message) {
        this.htmlMessageField = new SimpleStringProperty(message);
    }

    /**
     * sets a default value
     */
    public HTMLEditorBean() {
        this("");
    }

    public String getHtmlMessageField() {
        return this.htmlMessageField.get();
    }

    public void setHtmlMessageField(String message) {
        this.htmlMessageField.set(message);
    }

    public StringProperty getHtmlMessageFieldProperty() {
        return this.htmlMessageField;
    }
}
