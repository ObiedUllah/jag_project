package com.obied.beans.javaFX;

import java.io.File;
import java.util.List;
import javafx.beans.property.ListProperty;
import javafx.beans.property.SimpleListProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 * this javaFX bean will allow us to display the form information on top of the
 * htmleditor to the user
 *
 * @author Obied
 */
public class FormBean {

    private StringProperty to;
    private StringProperty cc;
    private StringProperty bcc;
    private StringProperty subject;
    private ListProperty attachments;

    /**
     * initializes the values
     *
     * @param to
     * @param cc
     * @param bcc
     * @param subject
     * @param attachments
     */
    public FormBean(String to, String cc, String bcc, String subject, ObservableList<File> attachments) {
        this.to = new SimpleStringProperty(to);
        this.cc = new SimpleStringProperty(cc);
        this.bcc = new SimpleStringProperty(bcc);
        this.subject = new SimpleStringProperty(subject);
        this.attachments = new SimpleListProperty(attachments);
    }

    /**
     * sets default values
     */
    public FormBean() {
        this("", "", "", "", FXCollections.observableArrayList());
    }

    public String getTo() {
        return to.get();
    }

    public void setTo(String to) {
        this.to.set(to);
    }

    public StringProperty getToProperty() {
        return to;
    }

    public String getCc() {
        return this.cc.get();
    }

    public void setCc(String cc) {
        this.cc.set(cc);
    }

    public StringProperty getCcProperty() {
        return cc;
    }

    public String getBcc() {
        return bcc.get();
    }

    public void setBcc(String bcc) {
        this.bcc.set(bcc);
    }

    public StringProperty getBccProperty() {
        return bcc;
    }

    public String getSubject() {
        return subject.get();
    }

    public void setSubject(String subject) {
        this.subject.set(subject);
    }

    public StringProperty getSubjectProperty() {
        return subject;
    }

    public ListProperty getAttachmentsProperty() {
        return attachments;
    }

    public List<File> getAttachments() {
        return attachments.getValue();
    }

    public void setAttachments(List<File> attachments) {
        this.attachments.setValue(attachments);
    }

}
