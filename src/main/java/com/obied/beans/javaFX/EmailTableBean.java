package com.obied.beans.javaFX;

import java.time.LocalDateTime;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 * This javaFX bean will allow us to display the emails fields needed in the
 * table
 *
 * @author Obied
 */
public class EmailTableBean {

    private IntegerProperty id;
    private StringProperty from;
    private StringProperty subject;
    private ObjectProperty date;

    /**
     * initializes the values
     *
     * @param id
     * @param from
     * @param subject
     * @param receive
     */
    public EmailTableBean(int id, String from, String subject, LocalDateTime receive) {
        this.id = new SimpleIntegerProperty(id);
        this.from = new SimpleStringProperty(from);
        this.subject = new SimpleStringProperty(subject);
        this.date = new SimpleObjectProperty(receive);

    }

    /**
     * sets default values
     */
    public EmailTableBean() {
        this(0, "", "", null);
    }

    public int getId() {
        return id.get();
    }

    public IntegerProperty getIdProperty() {
        return id;
    }

    public void setId(int id) {
        this.id.set(id);
    }

    public String getFrom() {
        return from.get();
    }

    public StringProperty getFromProperty() {
        return from;
    }

    public void setFrom(String from) {
        this.from.set(from);
    }

    public String getSubject() {
        return subject.get();
    }

    public StringProperty getSubjectProperty() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject.set(subject);
    }

    public LocalDateTime getDate() {
        return (LocalDateTime) date.get();
    }

    public ObjectProperty getDateProperty() {
        return date;
    }

    public void setDate(LocalDateTime date) {
        this.date.set(date);
    }

}
