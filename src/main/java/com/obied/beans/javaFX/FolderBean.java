package com.obied.beans.javaFX;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 * This JavaFX bean holds the info on the folder more importantly the folder name
 *
 * @author Obied
 */
public class FolderBean {

    private IntegerProperty id;
    private StringProperty folderName;

    /**
     * initializes the values
     * @param id
     * @param folderName 
     */
    public FolderBean(int id, String folderName) {
        this.id = new SimpleIntegerProperty(id);
        this.folderName = new SimpleStringProperty(folderName);
    }

    /**
     * sets default values
     */
    public FolderBean() {
        this(0, "");
    }

    public int getId() {
        return id.get();
    }

    public void setId(int id) {
        this.id.set(id);
    }

    public IntegerProperty getIdProperty() {
        return id;
    }

    public String getFolderName() {
        return folderName.get();
    }

    public StringProperty getFolderNameProperty() {
        return folderName;
    }

    public void setFolderName(String folderName) {
        this.folderName.set(folderName);
    }

}
