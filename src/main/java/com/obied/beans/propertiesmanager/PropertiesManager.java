package com.obied.beans.propertiesmanager;

import com.obied.beans.MailConfigBean;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import static java.nio.file.Files.newInputStream;
import static java.nio.file.Files.newOutputStream;
import java.nio.file.Path;
import static java.nio.file.Paths.get;
import java.util.Properties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * class to manage properties of the mailConfigBean. it will allow it to load
 * properties into a file or write them into a file
 *
 *
 * @author Obied Ullah
 *
 */
public class PropertiesManager {

    private final static Logger LOG = LoggerFactory.getLogger(PropertiesManager.class);

    /**
     * Updates a PropertyBean object with the contents of the properties file
     *
     * @param mailConfigBean
     * @param path
     * @param propFileName
     * @return
     * @throws java.io.IOException
     */
    public final boolean loadTextProperties(final MailConfigBean mailConfigBean, final String path, final String propFileName) throws IOException {

        boolean found = false;
        Properties prop = new Properties();

        Path txtFile = get(path, propFileName + ".properties");

        // File must exist
        if (Files.exists(txtFile)) {
            try ( InputStream propFileStream = newInputStream(txtFile);) {
                prop.load(propFileStream);
            }

            mailConfigBean.setUserName(prop.getProperty("userName"));
            mailConfigBean.setEmailAddress(prop.getProperty("emailAddress"));
            mailConfigBean.setMailPassword(prop.getProperty("mailPassword"));
            mailConfigBean.setImapURL(prop.getProperty("imapURL"));
            mailConfigBean.setSmtpURL(prop.getProperty("smtpURL"));
            mailConfigBean.setImapPort(prop.getProperty("imapPort"));
            mailConfigBean.setSmtpPort(prop.getProperty("smtpPort"));
            mailConfigBean.setMysqlURL(prop.getProperty("mysqlURL"));
            mailConfigBean.setMysqlDatabase(prop.getProperty("mysqlDatabase"));
            mailConfigBean.setMysqlPort(prop.getProperty("mysqlPort"));
            mailConfigBean.setMysqlUser(prop.getProperty("mysqlUser"));
            mailConfigBean.setMysqlPassword(prop.getProperty("mysqlPassword"));

            found = isValid(mailConfigBean);
        }
        return found;
    }

    /**
     * this helper method will verify if all the fields are empty
     *
     * @param mailConfigBean to check
     * @return true if the fields are not empty, false if they are empty
     */
    private boolean isValid(MailConfigBean mailConfigBean) {
        boolean valid = !(mailConfigBean.getUserName().equals("")
                || mailConfigBean.getEmailAddress().equals("") || mailConfigBean.getMailPassword().equals("")
                || mailConfigBean.getImapURL().equals("") || mailConfigBean.getSmtpURL().equals("")
                || mailConfigBean.getImapPort().equals("") || mailConfigBean.getSmtpPort().equals("")
                || mailConfigBean.getMysqlURL().equals("") || mailConfigBean.getMysqlDatabase().equals("")
                || mailConfigBean.getMysqlPort().equals("") || mailConfigBean.getMysqlUser().equals("")
                || mailConfigBean.getMysqlPassword().equals(""));

        return valid;
    }

    /**
     * Creates a plain text properties file based on the parameters
     *
     * @param path Must exist, will not be created
     * @param propFileName Name of the properties file
     * @param mailConfigBean The bean to store into the properties
     * @throws IOException
     */
    public final void writeTextProperties(final String path, final String propFileName, final MailConfigBean mailConfigBean) throws IOException {

        Properties prop = new Properties();

        prop.setProperty("userName", mailConfigBean.getUserName());
        prop.setProperty("emailAddress", mailConfigBean.getEmailAddress());
        prop.setProperty("mailPassword", mailConfigBean.getMailPassword());
        prop.setProperty("imapURL", mailConfigBean.getImapURL());
        prop.setProperty("smtpURL", mailConfigBean.getSmtpURL());
        prop.setProperty("imapPort", mailConfigBean.getImapPort());
        prop.setProperty("smtpPort", mailConfigBean.getSmtpPort());
        prop.setProperty("mysqlURL", mailConfigBean.getMysqlURL());
        prop.setProperty("mysqlDatabase", mailConfigBean.getMysqlDatabase());
        prop.setProperty("mysqlPort", mailConfigBean.getMysqlPort());
        prop.setProperty("mysqlUser", mailConfigBean.getMysqlUser());
        prop.setProperty("mysqlPassword", mailConfigBean.getMysqlPassword());

        Path txtFile = get(path, propFileName + ".properties");

        // Creates the file or if file exists it is truncated to length of zero
        // before writing
        try ( OutputStream propFileStream = newOutputStream(txtFile)) {
            prop.store(propFileStream, "SMTP Properties");
        }

    }
}
