package com.obied.beans;

import com.obied.beans.javaFX.EmailTableBean;
import com.obied.beans.javaFX.FormBean;
import com.obied.beans.javaFX.HTMLEditorBean;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Objects;
import javax.activation.DataSource;
import jodd.mail.Email;
import jodd.mail.EmailAddress;
import jodd.mail.EmailAttachment;
import jodd.mail.EmailMessage;
import jodd.mail.ReceivedEmail;

/**
 * This bean holds the info to handle folder names and received dates. Includes
 * helper methods to transform emailbean to other kind of beans
 *
 * @author Obied
 */
public class EmailBean {

    private int id;
    private int folderKey;
    private LocalDateTime receivedDate;
    public Email email;

    /**
     * default value of constructor
     */
    public EmailBean() {
        this(0, 0, new Timestamp(0).toLocalDateTime(), new Email());
    }

    /**
     * initializes the id, folderkey and the email object for the bean
     *
     * @param id
     * @param folderKey
     * @param ldt
     * @param email
     */
    public EmailBean(int id, int folderKey, LocalDateTime ldt, Email email) {
        this.id = id;
        this.folderKey = folderKey;
        this.receivedDate = ldt;
        this.email = email;
    }

    /**
     * this method converts a received email to a regular email
     *
     * @param receivedEmail
     * @return
     */
    public Email convertToEmail(ReceivedEmail receivedEmail) {
        Email emailToReturn = Email.create().from(receivedEmail.from())
                .subject(receivedEmail.subject())
                .textMessage(receivedEmail.messages().get(0).getContent())
                .htmlMessage(receivedEmail.messages().get(1).getContent())
                .to(receivedEmail.to())
                .cc(receivedEmail.cc());
        emailToReturn.attachments(receivedEmail.attachments());
        return emailToReturn;

    }

    /**
     * transforms an EmailBean to an emailTableBean checks for the dates and
     * sets the date depending on wether its an email that has been
     * sent,received or is being written
     *
     * @return emailTableBean created from emailBean
     */
    public EmailTableBean toEmailTableBean() {

        EmailTableBean emailTableBean = new EmailTableBean();
        emailTableBean.setId(id);
        emailTableBean.setFrom(email.from().getEmail());
        emailTableBean.setSubject(email.subject());

        //check if the email is a draft 
        if (this.receivedDate == null && email.sentDate() == null) {
            emailTableBean.setDate(null);
            return emailTableBean;
        }
        //check if the email was  sent
        if (this.receivedDate == null) {
            emailTableBean.setDate(email.sentDate().toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime());
        } else { // if the email was received
            emailTableBean.setDate(this.receivedDate);
        }
        return emailTableBean;
    }

    /**
     * transforms an EmailBean to a FormBean
     *
     * @return FormBean created fromEmailBean
     */
    public FormBean toEmailFormBean() {
        FormBean formBean = new FormBean();
        formBean.setTo(StringBuilder(email.to()));
        formBean.setCc(StringBuilder(email.cc()));
        formBean.setBcc(StringBuilder(email.bcc()));
        formBean.setSubject(email.subject());
        return formBean;
    }

    /**
     * will concatenate all the emails address received depending their type
     * will add ; in between similar to outlook
     *
     * @param array of emails that are either to,cc,bcc
     * @return String with all the emails appended
     */
    private String StringBuilder(EmailAddress[] array) {
        StringBuilder returnString = new StringBuilder("");
        for (EmailAddress item : array) {
            returnString.append(item.getEmail()).append("; ");
        }

        return returnString.toString();
    }

    /**
     * transforms an EmailBean to an HTMLEditorBean
     *
     * @return HTMLEditorBean created from EmailBean
     */
    public HTMLEditorBean toHTMLEditorBean() {
        HTMLEditorBean htmlBean = new HTMLEditorBean();
        String html = "";
        //get the html message only 
        for (EmailMessage message : email.messages()) {
            if (message.getMimeType().equals("text/html")) {
                html = message.getContent();
            }
        }
        htmlBean.setHtmlMessageField(html);
        return htmlBean;
    }

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return the folderKey
     */
    public int getFolderKey() {
        return folderKey;
    }

    /**
     * @param folderKey the folderKey to set
     */
    public void setFolderKey(int folderKey) {
        this.folderKey = folderKey;
    }

    /**
     * @return the receivedDate
     */
    public LocalDateTime getReceivedDate() {
        return receivedDate;
    }

    /**
     * @param receivedDate the receivedDate to set
     */
    public void setReceivedDate(LocalDateTime receivedDate) {
        this.receivedDate = receivedDate;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 47 * hash + this.id;
        hash = 47 * hash + this.folderKey;
        hash = 47 * hash + Objects.hashCode(this.receivedDate);
        hash = 47 * hash + Objects.hashCode(this.email);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        final EmailBean other = (EmailBean) obj;
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        if (this.id != other.id) {
            return false;
        }
        if (this.folderKey != other.folderKey) {
            return false;
        }
        if (!Objects.equals(this.receivedDate, other.receivedDate)) {
            return false;
        }
        //check from address
        if (!Objects.equals(this.email.from().getEmail(), other.email.from().getEmail())) {
            return false;
        }
        //check subject
        if (!Objects.equals(this.email.subject(), other.email.subject())) {
            return false;
        }
        //check text msg
        if (!Objects.equals(this.email.messages().get(0).getContent(), other.email.messages().get(0).getContent())) {
            return false;
        }
        //check html msg
        if (!Objects.equals(this.email.messages().get(1).getContent(), other.email.messages().get(1).getContent())) {
            return false;
        }

        //check sentDate
        if (!Objects.equals(this.email.sentDate(), other.email.sentDate())) {
            return false;
        }
        //check attachment
        for (EmailAttachment<? extends DataSource> attachment : this.email.attachments()) {
            if (!Objects.equals(this.email.attachments().get(0).getName(), other.email.attachments().get(0).getName())) {
                return false;
            }
        }
        //check to length
        if (this.email.to().length == other.email.to().length) {
            //check to
            for (int i = 0; i < this.email.to().length; i++) {
                if (!Objects.equals(this.email.to()[i].getEmail(), other.email.to()[i].getEmail())) {
                    return false;
                }
            }
        } else {
            return false;
        }
        //check cc length
        if (this.email.cc().length == other.email.cc().length) {
            //check cc
            for (int i = 0; i < this.email.cc().length; i++) {
                if (!Objects.equals(this.email.cc()[i].getEmail(), other.email.cc()[i].getEmail())) {
                    return false;
                }
            }
        } else {
            return false;
        }
        //check bcc
        //check if not null since only sent and draft emails can  be seen by the user
        if (this.receivedDate != null) {
            //check bcc length
            if (this.email.bcc().length == other.email.bcc().length) {
                //check bcc
                for (int i = 0; i < this.email.bcc().length; i++) {
                    if (!Objects.equals(this.email.bcc()[i].getEmail(), other.email.bcc()[i].getEmail())) {
                        return false;
                    }
                }
            } else { // different length
                return false;
            }
        }
        return true;
    }

    @Override
    public String toString() {
        return "EmailBean{" + "id=" + id + ", folderKey=" + folderKey + ", receivedDate=" + receivedDate + ", email=" + email + '}';
    }

}
