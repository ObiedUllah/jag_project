package com.obied.controllers;


import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.scene.web.WebView;

public class AboutWebViewController{

    @FXML // ResourceBundle that was given to the FXMLLoader
    private ResourceBundle resources;

    @FXML // URL location of the FXML file that was given to the FXMLLoader
    private URL location;

    @FXML // fx:id="webView"
    private WebView webView; // Value injected by FXMLLoader

    @FXML // This method is called by the FXMLLoader when initialization is complete
    void initialize() {
        assert webView != null : "fx:id=\"webView\" was not injected: check your FXML file 'AboutWebView.fxml'.";
        
        final String html = "about.html";
        final java.net.URI uri = java.nio.file.Paths.get(html).toAbsolutePath().toUri();

        // create WebView with specified local content
        webView.getEngine().load(uri.toString());

        
    }
}