package com.obied.controllers.config;

import com.obied.beans.MailConfigBean;
import com.obied.beans.propertiesmanager.PropertiesManager;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.beans.binding.Bindings;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

/**
 * this controller displays the form to put the mailconfigbean information. it
 * has an onclick that checks the fields and then will close the window and a
 * new root layout window will be created in the mainapp or from the root layout
 * itself
 *
 * @author Obied
 */
public class EmailFXMailConfigFormController {

    @FXML // ResourceBundle that was given to the FXMLLoader
    private ResourceBundle resources;

    @FXML // URL location of the FXML file that was given to the FXMLLoader
    private URL location;

    @FXML // fx:id="username"
    private TextField username; // Value injected by FXMLLoader

    @FXML // fx:id="email"
    private TextField email; // Value injected by FXMLLoader

    @FXML // fx:id="imapURL"
    private TextField imapURL; // Value injected by FXMLLoader

    @FXML // fx:id="smtpURL"
    private TextField smtpURL; // Value injected by FXMLLoader

    @FXML // fx:id="imapPort"
    private TextField imapPort; // Value injected by FXMLLoader

    @FXML // fx:id="smtpPort"
    private TextField smtpPort; // Value injected by FXMLLoader

    @FXML // fx:id="mysqlURL"
    private TextField mysqlURL; // Value injected by FXMLLoader

    @FXML // fx:id="mysqlDatabase"
    private TextField mysqlDatabase; // Value injected by FXMLLoader

    @FXML // fx:id="mysqlPort"
    private TextField mysqlPort; // Value injected by FXMLLoader

    @FXML // fx:id="mysqlUser"
    private TextField mysqlUser; // Value injected by FXMLLoader

    @FXML // fx:id="password"
    private PasswordField password; // Value injected by FXMLLoader

    @FXML // fx:id="mysqlPassword"
    private PasswordField mysqlPassword; // Value injected by FXMLLoader

    @FXML
    private Button saveButton;

    private MailConfigBean bean;
    private PropertiesManager manager;

    /**
     * when the config is saved, it will get the mailconfigbean from the
     * properties file and it will make sure that the fields are not empty
     * before closing the window.
     *
     * @param event
     * @throws IOException
     */
    @FXML
    void onClick(ActionEvent event) throws IOException {
        manager.writeTextProperties("", "mailConfig", bean);

        //make sure fields are not empty
        boolean checkFields = this.username.getText().equals("") || this.email.getText().equals("")
                || this.password.getText().equals("") || this.imapURL.getText().equals("")
                || this.smtpURL.getText().equals("") || this.imapPort.getText().equals("")
                || this.smtpPort.getText().equals("") || this.mysqlURL.getText().equals("")
                || this.mysqlDatabase.getText().equals("") || this.mysqlPort.getText().equals("")
                || this.mysqlUser.getText().equals("") || this.mysqlPassword.getText().equals("");
        if (checkFields) {
            //show and alert to ask for input
            Alert alert = new Alert(AlertType.ERROR);
            alert.setContentText("You cannot leave the fields empty");
            alert.show();
            return;
        }

        Stage stage = (Stage) this.saveButton.getScene().getWindow();
        stage.close();
    }

    @FXML // This method is called by the FXMLLoader when initialization is complete
    void initialize() {
        assert username != null : "fx:id=\"username\" was not injected: check your FXML file 'EmailFXLoginLayout.fxml'.";
        assert email != null : "fx:id=\"email\" was not injected: check your FXML file 'EmailFXLoginLayout.fxml'.";
        assert imapURL != null : "fx:id=\"imapURL\" was not injected: check your FXML file 'EmailFXLoginLayout.fxml'.";
        assert smtpURL != null : "fx:id=\"smtpURL\" was not injected: check your FXML file 'EmailFXLoginLayout.fxml'.";
        assert imapPort != null : "fx:id=\"imapPort\" was not injected: check your FXML file 'EmailFXLoginLayout.fxml'.";
        assert smtpPort != null : "fx:id=\"smtpPort\" was not injected: check your FXML file 'EmailFXLoginLayout.fxml'.";
        assert mysqlURL != null : "fx:id=\"mysqlURL\" was not injected: check your FXML file 'EmailFXLoginLayout.fxml'.";
        assert mysqlDatabase != null : "fx:id=\"mysqlDatabase\" was not injected: check your FXML file 'EmailFXLoginLayout.fxml'.";
        assert mysqlPort != null : "fx:id=\"mysqlPort\" was not injected: check your FXML file 'EmailFXLoginLayout.fxml'.";
        assert mysqlUser != null : "fx:id=\"mysqlUser\" was not injected: check your FXML file 'EmailFXLoginLayout.fxml'.";
        assert mysqlPassword != null : "fx:id=\"mysqlPassword\" was not injected: check your FXML file 'EmailFXLoginLayout.fxml'.";
        assert password != null : "fx:id=\"password\" was not injected: check your FXML file 'EmailFXLoginLayout.fxml'.";

    }

    /**
     * this will initialize the PropertiesManager and MailConfigBean
     *
     * @param manager
     * @param mailBean
     */
    public void setUpProperties(PropertiesManager manager, MailConfigBean mailBean) {
        this.bean = mailBean;
        this.manager = manager;
        FieldBindings();
    }

    /**
     * binds the fields
     */
    private void FieldBindings() {
        Bindings.bindBidirectional(this.username.textProperty(), bean.userNameProperty());
        Bindings.bindBidirectional(this.email.textProperty(), bean.emailAddressProperty());
        Bindings.bindBidirectional(this.password.textProperty(), bean.mailPasswordProperty());
        Bindings.bindBidirectional(this.imapURL.textProperty(), bean.imapURLProperty());
        Bindings.bindBidirectional(this.smtpURL.textProperty(), bean.smtpURLProperty());
        Bindings.bindBidirectional(this.imapPort.textProperty(), bean.imapPortProperty());
        Bindings.bindBidirectional(this.smtpPort.textProperty(), bean.smtpPortProperty());
        Bindings.bindBidirectional(this.mysqlURL.textProperty(), bean.mysqlURLProperty());
        Bindings.bindBidirectional(this.mysqlDatabase.textProperty(), bean.mysqlDatabaseProperty());
        Bindings.bindBidirectional(this.mysqlPort.textProperty(), bean.mysqlPortProperty());
        Bindings.bindBidirectional(this.mysqlUser.textProperty(), bean.mysqlUserProperty());
        Bindings.bindBidirectional(this.mysqlPassword.textProperty(), bean.mysqlPasswordProperty());
    }

    /**
     * returns the mailConfigBean
     *
     * @return MailConfigBean
     */
    public MailConfigBean getMailConfigBean() {
        return bean;
    }
}
