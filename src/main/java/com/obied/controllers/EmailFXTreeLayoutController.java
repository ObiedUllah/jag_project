package com.obied.controllers;

import com.obied.beans.EmailBean;
import com.obied.beans.javaFX.FolderBean;
import com.obied.exception.CannotRenameFolderException;
import com.obied.exception.FolderAlreadyExistsException;
import com.obied.exception.InvalidDeleteFolderException;
import com.obied.persistence.EmailDAO;
import java.net.URL;
import java.sql.SQLException;
import java.util.Objects;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.logging.Level;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonType;
import javafx.scene.control.DialogPane;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.control.TreeCell;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeView;
import javafx.scene.image.ImageView;
import javafx.scene.input.DragEvent;
import javafx.scene.input.Dragboard;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.TransferMode;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This controller is what stores the folder tree which is contained in the left
 * split of the root.
 *
 * It has multiple methods that will select the item selected inside the tree
 * and will display the table from the displayTable controller
 *
 * @author Obied
 */
public class EmailFXTreeLayoutController {

    private final static Logger LOG = LoggerFactory.getLogger(EmailFXTreeLayoutController.class);

    @FXML // ResourceBundle that was given to the FXMLLoader
    private ResourceBundle resources;

    @FXML // URL location of the FXML file that was given to the FXMLLoader
    private URL location;

    @FXML // fx:id="borderPaneTree"
    private BorderPane borderPaneTree; // Value injected by FXMLLoader

    @FXML // fx:id="treeView"
    private TreeView<FolderBean> treeView; // Value injected by FXMLLoader

    private EmailDAO emailDao;

    private EmailFXTableLayoutController tableLayoutController;

    private EmailFXHTMLLayoutController htmlLayoutController;

    private static final int INBOX_KEY = 1;

    /**
     * create a new folder
     *
     * will create an alert that gives an option to create a new email
     *
     * @param event
     */
    @FXML
    void onMouseClickCreate(MouseEvent event) {
        //create an alert 
        Alert alert = new Alert(AlertType.NONE);
        alert.setTitle(resources.getString("addFolder"));
        DialogPane dialogPane = alert.getDialogPane();

        //set alert text
        Label label = new Label(resources.getString("folderName"));
        TextField txtField = new TextField();
        VBox vbox = new VBox(label, txtField);
        dialogPane.setContent(vbox);

        //set the alert buttons
        alert.getButtonTypes().clear();
        alert.getButtonTypes().add(ButtonType.OK);
        alert.getButtonTypes().add(ButtonType.CANCEL);
        Optional<ButtonType> result = alert.showAndWait();
        //check if the user confirms their action
        if (result.get() == ButtonType.OK) {
            try {
                emailDao.createFolder(txtField.getText());
            } catch (FolderAlreadyExistsException | java.sql.SQLIntegrityConstraintViolationException e) {
                alertHandlerForExceptions(txtField.getText(), "folderExists", "folderExistsMsg");
            } catch (SQLException ex) {
                errorAlert(resources.getString("invalidSqlMsg"));
            }
            //display the new folder created
            displayTree();
        }
    }

    /**
     * handles the dropping of an email to a new folder
     *
     * @param event
     */
    @FXML
    void onDragDrop(DragEvent event) {
        Dragboard db = event.getDragboard();
        boolean success = false;
        LOG.debug("dropped");
        if (event.getDragboard().hasString()) {
            try {
                //get the email id we from the drag
                int emailId = Integer.parseInt(db.getString());
                LOG.debug("the email id is: " + emailId);
                //get the email itself
                EmailBean email = emailDao.findById(emailId);

                LOG.debug(" email = " + email.toString());
                LOG.debug("The event target: " + event.getTarget().toString());
                //get the foldername it has been dragged to
                String folderName = event.getTarget().toString().split("\"")[1];
                LOG.debug("the folder name is: " + folderName);
                //make sure its not the draft folder
                if (!folderName.contentEquals("Draft")) {
                    //get the new folder's id
                    int newId = emailDao.findFolderId(folderName);
                    LOG.debug("the newid  is: " + newId);
                    //update the folder
                    email.setFolderKey(newId);
                    emailDao.updateFolder(email);
                } else {
                    //create a alert that you cannot put an email into drafts
                    alertHandlerForExceptions("", "cantDropDraft", "cantDropDraftMsg");
                }
            } catch (SQLException e) {
                LOG.debug("EXCEPTION THROWN: " + e);
            }
            success = true;

        }
        event.setDropCompleted(success);
        event.consume();
    }

    /**
     * make sure items can be dragged over
     *
     * @param event
     */
    @FXML
    void onDragOver(DragEvent event) {

        // Accept it only if it is not dragged from the same control and if it
        // has a string data
        if (event.getGestureSource() != treeView && event.getDragboard().hasString()) {
            // allow for both copying and moving, whatever user chooses
            event.acceptTransferModes(TransferMode.COPY_OR_MOVE);
        }
        event.consume();

    }

    /**
     * handles the deletion of a folder
     *
     * creates an alert making sure the user wants to delete the folder
     *
     * @param event
     */
    @FXML
    void onMouseClickDelete(MouseEvent event) {
        //get the folder name
        String folder = treeView.getSelectionModel().selectedItemProperty().get().getValue().getFolderName();
        LOG.debug("name of folder trying to delete : " + folder);

        //create an alert 
        Alert alert = new Alert(AlertType.NONE);
        alert.setTitle(resources.getString("deleteFolder"));
        DialogPane dialogPane = alert.getDialogPane();

        //set alert text
        Label label = new Label(resources.getString("deleteConfirmation") + " " + folder);
        dialogPane.setContent(label);

        //set alert buttons
        alert.getButtonTypes().clear();
        alert.getButtonTypes().add(ButtonType.OK);
        alert.getButtonTypes().add(ButtonType.CANCEL);
        Optional<ButtonType> result = alert.showAndWait();

        //check if the user confirms their action
        if (result.get() == ButtonType.OK) {
            try {
                emailDao.deleteFolder(folder);
            } catch (InvalidDeleteFolderException e) {
                alertHandlerForExceptions(folder, "cantDeleteFolder", "cantDeleteFolderMsg");
            } catch (SQLException ex) {
                errorAlert(resources.getString("invalidSqlMsg"));
            }
            displayTree();
        }
    }

    /**
     * rename an email
     *
     * creates an alert making sure the user wants to rename the folder
     *
     * @param event
     */
    @FXML
    void onMouseClickRename(MouseEvent event) {
        //get the orginal foldername
        String folder = treeView.getSelectionModel().selectedItemProperty().get().getValue().getFolderName();
        LOG.debug("name of folder trying to rename : " + folder);

        //create alert
        Alert alert = new Alert(AlertType.NONE);
        alert.setTitle(resources.getString("renameFolder"));
        DialogPane dialogPane = alert.getDialogPane();

        //set alert text
        Label label = new Label(resources.getString("renameFolderMsg") + " " + folder + " to:");
        TextField txtField = new TextField();
        VBox vbox = new VBox(label, txtField);
        dialogPane.setContent(vbox);

        //set alert buttons
        alert.getButtonTypes().clear();
        alert.getButtonTypes().add(ButtonType.OK);
        alert.getButtonTypes().add(ButtonType.CANCEL);
        Optional<ButtonType> result = alert.showAndWait();

        //check if the user confirms their action
        if (result.get() == ButtonType.OK) {
            try {
                emailDao.renameFolder(folder, txtField.getText());
            } catch (CannotRenameFolderException e) {
                alertHandlerForExceptions(folder, "cantRenameFolder", "cantRenameFolderMsg");
            } catch (FolderAlreadyExistsException e) {
                alertHandlerForExceptions(txtField.getText(), "folderExists", "folderExistsMsg");
            } catch (SQLException ex) {
                errorAlert(resources.getString("invalidSqlMsg"));
            }
            displayTree();
        }
    }

    @FXML // This method is called by the FXMLLoader when initialization is complete
    void initialize() {

        assert borderPaneTree != null : "fx:id=\"borderPaneTree\" was not injected: check your FXML file 'EmailFXTreeLayout.fxml'.";
        assert treeView != null : "fx:id=\"treeView\" was not injected: check your FXML file 'EmailFXTreeLayout.fxml'.";

        FolderBean folder = new FolderBean();

        //set the root to know we are dealing with folder beans
        treeView.setRoot(new TreeItem<>(folder));

        // This cell factory is used to choose which field in the FolderData object
        // is used for the node name
        treeView.setCellFactory((e) -> new TreeCell<FolderBean>() {
            @Override
            protected void updateItem(FolderBean item, boolean empty) {
                super.updateItem(item, empty);
                if (Objects.nonNull(item)) {
                    setText(item.getFolderName());
                    setGraphic(getTreeItem().getGraphic());
                } else {
                    setText("");
                    setGraphic(null);
                }
            }
        });

    }

    /**
     * sets the emailDAO
     *
     * @param emailDao
     */
    public void setEmailDAO(EmailDAO emailDao) {
        this.emailDao = emailDao;
    }

    /**
     * sets the tablecontroller
     *
     * @param tableLayoutController
     */
    public void setTableController(EmailFXTableLayoutController tableLayoutController) {
        this.tableLayoutController = tableLayoutController;
    }

    /**
     * sets the html layout for the tree
     *
     * @param htmlLayoutController
     */
    public void setHTMLLayoutController(EmailFXHTMLLayoutController htmlLayoutController) {
        this.htmlLayoutController = htmlLayoutController;
    }

    /**
     * Build the tree from the database, when first opening, it will already
     * select the inbox folder and the emails in it
     *
     */
    public void displayTree() {
        //get the position of the current selected item in the tree
        int position = treeView.getSelectionModel().getSelectedIndex();

        treeView.getRoot().getChildren().clear();
        // Retrieve the list of fish
        ObservableList<FolderBean> folders;
        try {
            folders = FXCollections.observableArrayList(emailDao.findAllFolders());
            addToTreeViewRoot(folders);
        } catch (SQLException ex) {
            errorAlert(resources.getString("invalidSqlMsg"));
        }

        // Open the tree
        treeView.getRoot().setExpanded(true);

        selectedItemListener();

        setItemPosition(position);
    }

    /**
     * sets the position of the item in the tree to the same one as before or
     * the inbox folder one
     *
     * @param position
     */
    public void setItemPosition(int position) {
        LOG.debug("position: " + position);
        LOG.debug("amt of folders: " + treeView.getRoot().getChildren().size());

        //treeView.getSelectionModel().clearSelection();
        //set the position of the item back to its original position
        //if position = 1, -1, or is bigger than the total amt of folders than set it to inbox
        if (position == 0 || position == -1 || position > treeView.getRoot().getChildren().size()) {
            treeView.getSelectionModel().select(INBOX_KEY);
        } else {
            treeView.getSelectionModel().select(position);
        }
    }

    /**
     * This will add all the items from the observable list into the the
     * treeView
     *
     * @param folders
     */
    private void addToTreeViewRoot(ObservableList<FolderBean> folders) {
        //set the inbox folder first
        SetInboxFolder(folders);
        //set the draft and sent folder to second and third
        SetDraftSentFolder(folders);

        // Build an item for each folder and add it to the root
        if (Objects.nonNull(folders)) {
            folders.stream().map((fb) -> new TreeItem<>(fb)).map((item) -> {
                item.setGraphic(new ImageView(getClass().getResource("/images/folder.png").toExternalForm()));
                return item;
            }).forEach((item) -> {
                if (!item.getValue().getFolderName().contentEquals("Inbox") && !item.getValue().getFolderName().contentEquals("Draft") && !item.getValue().getFolderName().contentEquals("Sent")) {
                    treeView.getRoot().getChildren().add(item);
                }
            });
        }
    }

    /**
     * this will set the inbox folder as the first one
     *
     * @param folders
     */
    private void SetInboxFolder(ObservableList<FolderBean> folders) {
        // Build an item for each folder and add it to the root
        if (Objects.nonNull(folders)) {
            folders.stream().map((fb) -> new TreeItem<>(fb)).map((item) -> {
                item.setGraphic(new ImageView(getClass().getResource("/images/folder.png").toExternalForm()));
                return item;
            }).forEach((item) -> {
                //add the inbox folder first
                if (item.getValue().getFolderName().contentEquals("Inbox")) {
                    treeView.getRoot().getChildren().add(item);
                }
            });
        }
    }

    /**
     * this will set the draft and sent folder as the second and third one
     *
     * @param folders
     */
    private void SetDraftSentFolder(ObservableList<FolderBean> folders) {
        // Build an item for each folder and add it to the root
        if (Objects.nonNull(folders)) {
            folders.stream().map((fb) -> new TreeItem<>(fb)).map((item) -> {
                item.setGraphic(new ImageView(getClass().getResource("/images/folder.png").toExternalForm()));
                return item;
            }).forEach((item) -> {
                //add the second and third folder to draft and sent
                if (item.getValue().getFolderName().contentEquals("Draft") || item.getValue().getFolderName().contentEquals("Sent")) {
                    treeView.getRoot().getChildren().add(item);
                }
            });
        }
    }

    /**
     * Creates a listener for when a option in the tree (TreeItem) is clicked
     */
    private void selectedItemListener() {
        // Listen for selection changes and show the EmailTableBean details when
        // changed.
        treeView.getSelectionModel().selectedItemProperty()
                .addListener((observable, oldValue, newValue) -> {
                    showEmailDataTree(newValue);
                    // when clicking an email, we want to set the values of the form and html back to default
                    this.htmlLayoutController.disableFieldsForTree();
                });

    }

    /**
     * calls the displayTable from controller
     *
     * @param folder
     */
    private void showEmailDataTree(TreeItem<FolderBean> folder) {
        try {
            tableLayoutController.displayTable(folder.getValue().getId());
            LOG.info("selected item: " + folder.getValue().getFolderName());
        } catch (NullPointerException ne) {
            //the user clicked for example the spacebar in the beginning so it throws a null pointer
            //should be ignored
        }

    }

    /**
     * Error message popup dialog
     *
     * @param msg
     */
    private void errorAlert(String msg) {
        Alert dialog = new Alert(Alert.AlertType.ERROR);
        dialog.setTitle(resources.getString("sqlError"));
        dialog.setHeaderText(resources.getString("sqlError"));
        dialog.setContentText(msg);
        dialog.show();
    }

    /**
     * similar alert for many self-made exceptions thrown in the dao
     *
     * @param foldername
     * @param header
     * @param content
     */
    private void alertHandlerForExceptions(String foldername, String header, String content) {
        Alert dialog = new Alert(Alert.AlertType.ERROR);
        dialog.setTitle(resources.getString("error"));
        dialog.setHeaderText(resources.getString(header));
        dialog.setContentText(resources.getString(content) + foldername);
        dialog.show();
    }

}
