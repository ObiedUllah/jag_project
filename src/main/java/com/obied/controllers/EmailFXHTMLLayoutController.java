package com.obied.controllers;

import com.obied.beans.EmailBean;
import com.obied.beans.MailConfigBean;
import com.obied.beans.javaFX.FormBean;
import com.obied.beans.javaFX.HTMLEditorBean;
import com.obied.business.SendAndReceive;
import com.obied.exception.InvalidEmailFormatException;
import com.obied.exception.NoEmailFoundException;
import com.obied.persistence.EmailDAO;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.URL;
import java.nio.file.Files;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.binding.Bindings;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.DialogPane;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.web.HTMLEditor;
import javafx.stage.DirectoryChooser;
import javafx.stage.FileChooser;
import javafx.stage.Modality;
import javafx.stage.Stage;
import jodd.mail.EmailAttachment;
import org.slf4j.LoggerFactory;

/**
 * this class will: display the different emails content inside the form and the
 * html editor write into fields to create a new draft. write into fields to
 * send a new email. save an attachment insert an attachment are also available.
 * to screate an email the user need to click compose
 *
 * @author Obied
 */
public class EmailFXHTMLLayoutController {

    private final static org.slf4j.Logger LOG = LoggerFactory.getLogger(EmailFXHTMLLayoutController.class);

    @FXML // ResourceBundle that was given to the FXMLLoader
    private ResourceBundle resources;

    @FXML // URL location of the FXML file that was given to the FXMLLoader
    private URL location;

    @FXML // fx:id="htmlEditor"
    private HTMLEditor htmlEditor; // Value injected by FXMLLoader

    @FXML // fx:id="toField"
    private TextField toField; // Value injected by FXMLLoader

    @FXML // fx:id="ccField"
    private TextField ccField; // Value injected by FXMLLoader

    @FXML // fx:id="bccField"
    private TextField bccField; // Value injected by FXMLLoader

    @FXML // fx:id="subjectField"
    private TextField subjectField; // Value injected by FXMLLoader

    private EmailDAO emailDao;

    private EmailFXTableLayoutController tableLayoutController;

    private MailConfigBean mailConfigBean;

    private FormBean formBean;

    private HTMLEditorBean htmlEditorBean;

    private EmailFXTreeLayoutController treeLayoutController;

    private static final int SENT_KEY = 2;
    private static final int DRAFT_KEY = 3;
    private static final int SENT_POSITION = 3;
    private static final int DRAFT_POSITION = 2;

    /**
     * delete the current email and make a alert to make sure they want to
     * delete it
     *
     * @param event
     */
    @FXML
    void onClickDelete(MouseEvent event) {
        //check if an email is selected
        if (tableLayoutController.getEmailDataTable().getSelectionModel().selectedItemProperty().getValue() != null) {

            //create an alert 
            Alert alert = new Alert(Alert.AlertType.NONE);
            alert.setTitle(resources.getString("deleteEmail"));
            DialogPane dialogPane = alert.getDialogPane();

            //set alert text 
            Label label = new Label(resources.getString("deleteEmailConfirmation"));
            dialogPane.setContent(label);

            //set alert buttons
            alert.getButtonTypes().clear();
            alert.getButtonTypes().add(ButtonType.OK);
            alert.getButtonTypes().add(ButtonType.CANCEL);
            Optional<ButtonType> result = alert.showAndWait();

            //check if the user confirms their action
            if (result.get() == ButtonType.OK) {
                try {
                    //get the emailBean selected
                    EmailBean emailBean = tableLayoutController.getEmailDataTable().getSelectionModel().selectedItemProperty().get();
                    LOG.debug("ITEM IS SELECTED, EMAILBEAN IS  " + emailBean);

                    //delete the email
                    emailDao.delete(emailBean.getId());
                    //display the folders without the deleted one
                    tableLayoutController.displayTable(emailBean.getFolderKey());
                } catch (SQLException ex) {
                    errorAlert(resources.getString("invalidSqlMsg"));
                }
            }
        }
    }

    /**
     * save a draft. you can save any email as a draft.
     *
     * @param event
     */
    @FXML
    void onClickSaveDraft(MouseEvent event) {
        LOG.debug("SAVE DRAFT BUTTON CLICKED ");
        EmailBean emailBean = new EmailBean();
        try {
            //check if an email is selected
            if (tableLayoutController.getEmailDataTable().getSelectionModel().selectedItemProperty().getValue() != null) {
                //get the current emailBean selected
                emailBean = tableLayoutController.getEmailDataTable().getSelectionModel().selectedItemProperty().get();

                //check if the email is a draft
                if (emailBean.getFolderKey() == DRAFT_KEY) {
                    LOG.debug("UPDATING PREVIOUS DRAFT");
                    LOG.debug("email id inside save draft: " + emailBean.getId());
                    //updating draft
                    emailDao.updateDraft(createEmail(emailBean));
                    this.formBean.setAttachments(FXCollections.observableArrayList());

                } else {
                    //checks if the email is a reply and being saved as a draft
                    LOG.debug("CREATING A DRAFT FROM REPLY ");
                    emailBean = new EmailBean();

                    //create a new draft 
                    emailDao.createEmail(createEmail(emailBean), DRAFT_KEY);
                    treeLayoutController.setItemPosition(DRAFT_POSITION);
                    this.formBean.setAttachments(FXCollections.observableArrayList());

                }

            } //if an email is not selected but there is content in at least a field, then we create a new draft
            else if ((!toField.getText().isEmpty() || !ccField.getText().isEmpty() || !bccField.getText().isEmpty() || !subjectField.getText().isEmpty())) {
                LOG.debug("CREATING DRAFT ");
                //create a new draft
                emailDao.createEmail(createEmail(emailBean), DRAFT_KEY);
                treeLayoutController.setItemPosition(DRAFT_POSITION);
                this.formBean.setAttachments(FXCollections.observableArrayList());

            }
        } catch (SQLException ex) {
            errorAlert(resources.getString("invalidSqlMsg"));
        }

    }

    /**
     * insert an attachment to an email only if its a new email being written
     *
     * @param event
     */
    @FXML
    void onInsertAttachment(ActionEvent event) {
        EmailBean selectedBean = tableLayoutController.getEmailDataTable().getSelectionModel().selectedItemProperty().getValue();
        //run only if its a previous draft or a completely new email 
        if (Objects.isNull(selectedBean) || selectedBean.getFolderKey() == DRAFT_KEY || !subjectField.isDisabled()) {
            FileChooser fileChooser = new FileChooser();
            Stage stage = new Stage();
            stage.initModality(Modality.APPLICATION_MODAL);
            File file = fileChooser.showOpenDialog(stage);
            //add to attachments and display image only if a file is selected
            if (Objects.nonNull(file)) {
                //add attachments to formbean attachments
                formBean.getAttachments().add(file);
                //display the inserted image
                displayImagesInHtml(file);
            }
        }

    }

    /**
     * reply to an email and make sure it can be replied to
     *
     * @param event
     */
    @FXML
    void onMouseClickReply(MouseEvent event) {
        LOG.debug("ON REPLY CLICKED");
        //get the selected email bean
        EmailBean emailBean = tableLayoutController.getEmailDataTable().getSelectionModel().selectedItemProperty().getValue();

        //make sure the selected email to reply to is not null nd that its not a draft
        if (Objects.nonNull(emailBean) && emailBean.getFolderKey() != DRAFT_KEY) {
            //make it editable
            setDefaultFields();
            setFields(false);

            //set the fields to the ones from the email to reply to
            String emailTo = emailBean.email.from().getEmail();
            String subject = "RE: " + emailBean.email.subject();
            String message = "RE: " + emailBean.email.messages().get(0).getContent();

            toField.setText(emailTo);
            subjectField.setText(subject);
            htmlEditor.setHtmlText(message);

            //display the html editor
            displayHTML(emailBean);

        }
    }

    /**
     * send email depending on if its a new one or a reply or draft
     *
     * @param event
     */
    @FXML
    void onMouseClickSendEmail(MouseEvent event) {
        LOG.debug("SENT BUTTON CLICKED");
        SendAndReceive sendAndReceive = new SendAndReceive(this.mailConfigBean);

        EmailBean emailBean = new EmailBean();
        EmailBean selectedBean = tableLayoutController.getEmailDataTable().getSelectionModel().selectedItemProperty().getValue();
        //if its selected and its a draft, get previous values
        if (Objects.nonNull(selectedBean) && selectedBean.getFolderKey() == DRAFT_KEY) {
            LOG.debug("was a draft beforehand");
            emailBean = tableLayoutController.getEmailDataTable().getSelectionModel().selectedItemProperty().get();
            LOG.debug("ITEM IS SELECTED, EMAILBEAN IS  " + emailBean);
        }

        //run only if its a previous draft or a completely new email or a reply
        if (Objects.isNull(selectedBean) || selectedBean.getFolderKey() == DRAFT_KEY || !subjectField.isDisabled()) {
            LOG.debug("TO: " + toField.getText());
            LOG.debug("CC: " + ccField.getText());
            LOG.debug("BCC: " + bccField.getText());
            LOG.debug("SUBJECT: " + subjectField.getText());
            LOG.debug("HTML: " + htmlEditor.getHtmlText());

            //creating fields for sending email
            String subject = subjectField.getText();
            String message = htmlEditor.getHtmlText();

            //getting the attachments , only using regularattachments as asked
            List<File> regularAttachments = formBean.getAttachments();
            LOG.debug("attachments in form bean : " + formBean.getAttachments().toString());
            List<File> embeddedAttachments = new ArrayList();

            boolean isSent = false;
            try {
                //send the email 
                emailBean.email = sendAndReceive.send(splitList(toField), splitList(ccField), splitList(bccField), subject, message, message, regularAttachments, embeddedAttachments);
                emailBean.email.sentDate(new Date());
                LOG.debug("Date bing set to" + emailBean.email.sentDate().toString());
                isSent = true;
            } catch (FileNotFoundException e) {
                LOG.debug("FileNotFoundException");
            } catch (InvalidEmailFormatException e) {
                invalidEmailFormatAlert();
            } catch (NoEmailFoundException e) {
                noEmailFoundAlert();
            }
            //this will only be called if the email is sent
            addEmailToDb(emailBean, isSent);

            //reset the formbean after sending
            this.formBean.setAttachments(FXCollections.observableArrayList());
        }
    }

    /**
     * save attachments to a specific folder in the computer, note: it will also
     * save in the root folder
     *
     * @param event
     */
    @FXML
    void onSaveAttachment(ActionEvent event) {
        LOG.debug("ON SAVE ATTCHMENT CLICKED");
        if (Objects.nonNull(tableLayoutController.getEmailDataTable().getSelectionModel().selectedItemProperty().getValue()) && !formBean.getAttachments().isEmpty()) {
            try {
                //let the user choose the directory to save to
                DirectoryChooser directoryChooser = new DirectoryChooser();
                Stage stage = new Stage();
                stage.initModality(Modality.APPLICATION_MODAL);

                File directory = directoryChooser.showDialog(stage);
                saveAttachmentToFolder(directory, formBean.getAttachments());
            } catch (NullPointerException NE) {
                //the user exited the program so nothing should happen
            }
        }

    }

    /**
     * this will save all the attachments to a specific folder. note: this will
     * also save in the root folder
     *
     * @param directory
     * @param attachments
     */
    private void saveAttachmentToFolder(File directory, List<File> attachments) {
        LOG.debug("is it a directory: " + directory.isDirectory());
        //check if the selected item is a folder not a file
        if (directory.isDirectory()) {
            File file;
            OutputStream outputStream;
            LOG.debug("CHECKING ATTACHMENTS: " + directory.isDirectory());
            //go through all the attachments
            for (File attach : attachments) {
                LOG.debug("attachment names: " + attach.getName());
                file = new File(directory.getAbsolutePath() + "/" + attach.getName());
                try {
                    outputStream = new FileOutputStream(file);
                    outputStream.write(Files.readAllBytes(attach.toPath()));
                } catch (IOException ex) {
                    this.NoSuchFileAlert();

                    // if the file isnt saved in the root
                }
            }
        }
    }

    /**
     * to compose an email the user needs to click this button.
     *
     * @param event
     */
    @FXML
    void composeButton(ActionEvent event) {
        //enable the user to write
        this.setDefaultFields();
        this.setFields(false);
        this.formBean.setAttachments(FXCollections.observableArrayList());

        try {
            this.tableLayoutController.getEmailDataTable().getSelectionModel().clearSelection();
        } catch (NullPointerException ne) {
            //nothing is selected so its fine
        }

    }

    /**
     * sets emailDAO
     *
     * @param emaildao
     */
    public void setEmailDao(EmailDAO emaildao) {
        this.emailDao = emaildao;
    }

    @FXML // This method is called by the FXMLLoader when initialization is complete
    void initialize() {

        assert htmlEditor != null : "fx:id=\"htmlEditor\" was not injected: check your FXML file 'EmailFXHTMLLayout.fxml'.";
        assert toField != null : "fx:id=\"toField\" was not injected: check your FXML file 'EmailFXHTMLLayout.fxml'.";
        assert ccField != null : "fx:id=\"ccField\" was not injected: check your FXML file 'EmailFXHTMLLayout.fxml'.";
        assert bccField != null : "fx:id=\"bccField\" was not injected: check your FXML file 'EmailFXHTMLLayout.fxml'.";
        assert subjectField != null : "fx:id=\"subjectField\" was not injected: check your FXML file 'EmailFXHTMLLayout.fxml'.";

        formBean = new FormBean();
        htmlEditorBean = new HTMLEditorBean();
        //set the bindings 
        setBindings();
    }

    /**
     * uses a Listener to check when a email is clicked from the table in the
     * right upper split pane. then proceeds to call helper methods to display
     * the form and html
     */
    public void displayForm() {

        LOG.info(tableLayoutController.getEmailDataTable().getSelectionModel().selectedItemProperty().getName());
        tableLayoutController.getEmailDataTable().getSelectionModel().selectedItemProperty()
                .addListener((observable, oldValue, newValue) -> {
                    try {
                        //clear the form
                        setDefaultFields();
                        //set the new values of the form from what email was clicked
                        displayfields(newValue.getId());

                    } catch (NullPointerException ne) {
                        //it will throw an exception when a user click on a email first then a folder which is normal
                        //just ignore the exception
                    }
                });
    }

    /**
     * sets the mailconfigBean
     *
     * @param mailConfigBean
     */
    public void setMailConfigBean(MailConfigBean mailConfigBean) {
        this.mailConfigBean = mailConfigBean;
    }

    /**
     * sets table controller
     *
     * @param tableLayoutController
     */
    public void setTableController(EmailFXTableLayoutController tableLayoutController) {
        this.tableLayoutController = tableLayoutController;
    }

    /**
     * sets the tree layout controller
     *
     * @param treeLayoutController
     */
    public void setTreeLayoutController(EmailFXTreeLayoutController treeLayoutController) {
        this.treeLayoutController = treeLayoutController;
    }

    /**
     * this will display the to, cc, bcc, subject fields
     *
     * @param id
     */
    public void displayfields(int id) {
        LOG.debug("displaying fields");
        EmailBean emailBean = null;
        this.formBean.setAttachments(FXCollections.observableArrayList());

        try {
            emailBean = emailDao.findById(id);
            //set the texts from the bean
            formBean = emailBean.toEmailFormBean();
        } catch (SQLException ex) {
            errorAlert("displaying fields");
        }

        //if the date is  null then we want an empty string
        if (Objects.nonNull(emailBean.email.sentDate()) || (Objects.isNull(emailBean.email.sentDate()) && Objects.isNull(emailBean.getReceivedDate()))) {
            bccField.setText(formBean.getBcc());
        } else {
            bccField.setText("");
        }

        toField.setText(formBean.getTo());
        ccField.setText(formBean.getCc());
        subjectField.setText(formBean.getSubject());

        //set the attachments
        ObservableList<File> attachments = FXCollections.observableArrayList();
        emailBean.email.attachments().forEach(attachment -> {
            attachments.add(new File(attachment.getName()));
        });
        formBean.getAttachments().addAll(attachments);

        //display the html
        displayHTML(emailBean);

        //enable or disable fields depending on what kind of email it is
        enableFields(emailBean);
        disableFields(emailBean);

    }

    /**
     * sets the bingding for the form
     */
    private void setBindings() {
        Bindings.bindBidirectional(this.toField.textProperty(), formBean.getToProperty());
        Bindings.bindBidirectional(this.ccField.textProperty(), formBean.getCcProperty());
        Bindings.bindBidirectional(this.bccField.textProperty(), formBean.getBccProperty());
        Bindings.bindBidirectional(this.subjectField.textProperty(), formBean.getSubjectProperty());
    }

    /**
     * this will display the html field
     *
     * @param id
     */
    private void displayHTML(EmailBean emailBean) {
        htmlEditorBean = emailBean.toHTMLEditorBean();
        //remove all previous image 
        this.htmlEditor.setHtmlText(htmlEditorBean.getHtmlMessageField().replaceAll("\\<.*?\\>", ""));
        //for every attachment we want to append them 
        for (EmailAttachment attach : emailBean.email.attachments()) {
            this.displayImagesInHtml(new File(attach.getName()));
        }
    }

    /**
     *
     * disable html and form if the email is not a draft
     *
     * @param emailBean
     */
    private void disableFields(EmailBean emailBean) {
        if (Objects.nonNull(emailBean.getReceivedDate()) || Objects.nonNull(emailBean.email.sentDate())) {
            setFields(true);
        }
    }

    /**
     * enable the fields if the email is a draft
     *
     * @param emailBean
     */
    private void enableFields(EmailBean emailBean) {
        LOG.debug("ITEM SELECTED IN ENABLED: " + tableLayoutController.getEmailDataTable().getSelectionModel().selectedItemProperty().getValue());
        if (Objects.isNull(emailBean.getReceivedDate()) && Objects.isNull(emailBean.email.sentDate())) {
            setFields(false);
        }
    }

    /**
     * this disables the field on the tree controller and sets the default
     * values to empty
     */
    public void disableFieldsForTree() {
        this.setDefaultFields();
        setFields(true);

    }

    /**
     * This method will either enable or disable the fields in the form and html
     *
     * @param bool
     */
    private void setFields(boolean bool) {
        htmlEditor.setDisable(bool);
        bccField.setDisable(bool);
        toField.setDisable(bool);
        ccField.setDisable(bool);
        subjectField.setDisable(bool);
    }

    /**
     * sets fields back to default
     */
    private void setDefaultFields() {
        htmlEditor.setHtmlText("");
        bccField.setText("");
        toField.setText("");
        ccField.setText("");
        subjectField.setText("");
    }

    /**
     * this fills an emailbean
     *
     * @param emailBean
     * @return
     */
    private EmailBean createEmail(EmailBean emailBean) {
        //adding recievers
        for (String to : splitList(toField)) {
            emailBean.email.to(to);
        }
        for (String cc : splitList(ccField)) {
            emailBean.email.cc(cc);
        }
        for (String bcc : splitList(bccField)) {
            emailBean.email.bcc(bcc);
        }

        //adding email content
        emailBean.email.subject(subjectField.getText());
        emailBean.email.from(this.mailConfigBean.getEmailAddress());
        emailBean.email.messages().clear();
        emailBean.email.textMessage("");
        emailBean.email.htmlMessage(this.htmlEditor.getHtmlText());

        //adding attachment content 
        for (File attach : formBean.getAttachments()) {
            try {
                LOG.debug("form bean attach: " + attach.getName());
                //adds content only if the file exists in root directory
                emailBean.email.attachment(EmailAttachment.with().content(attach.getName()));
            } // the email is being saved as a draft
            catch (jodd.mail.MailException ex) {
                //create the attachment before 
                createAttachmentForDraft(attach);

                //adding attachment content to the email
                emailBean.email.attachment(EmailAttachment.with().content(attach.getName()));

            }
        }
        this.formBean.setAttachments(FXCollections.observableArrayList());

        return emailBean;
    }

    private void createAttachmentForDraft(File attach) {
        //if the user tries to save a file in a draft, it won't be able to because the file doesn't exist on the root of the project
        //which is why i add the file to the root 
        File file;
        OutputStream outputStream;
        file = new File(attach.getName());
        try {
            //creating file content
            outputStream = new FileOutputStream(file);
            outputStream.write(Files.readAllBytes(attach.toPath()));
        } catch (FileNotFoundException ex) {
            //a file must be chosen 
        } catch (IOException ex) {
            //a fiile must be chosen
        }
    }

    /**
     * splits the content of a textfield into different string
     *
     * @param field
     * @return a list with all the different emails
     */
    private List<String> splitList(TextField field) {
        List<String> list = new ArrayList();
        //check if the label is not empty first
        if (!field.getText().isEmpty()) {
            //list = Arrays.asList(field.getText().replace("\\s+", ";").split("; "));
            list = Arrays.asList(field.getText().replace(" ", "").split(";"));
        }

        //check if the split worked
        for (String element : list) {
            LOG.debug("emails being sent to: " + element);
        }
        return list;
    }

    /**
     * this will add a sent email to the db. if the email already exists as a
     * draft. the draft is removed and a new sent email is created
     *
     * @param emailBean
     * @param isSent
     */
    private void addEmailToDb(EmailBean emailBean, boolean isSent) {
        boolean emailAdded = false;
        if (isSent) {
            //if the email is not a draft
            if (emailBean.getFolderKey() != DRAFT_KEY) {
                LOG.debug("is in sent , folder key: " + emailBean.getFolderKey());
                emailBean.setFolderKey(SENT_KEY);
                emailBean.setReceivedDate(null);
                try {
                    //create a new email in the db
                    emailDao.createEmail(emailBean, SENT_KEY);
                    //emailDao.saveBlobToDisk(emailBean.getId());
                } catch (SQLException ex) {
                    errorAlert(resources.getString("sqlError"));
                }

                emailAdded = true;
                LOG.debug("EMAIL HAS BEEN SENT");
            }

            //if email is a draft
            if (emailBean.getFolderKey() == DRAFT_KEY) {
                LOG.debug("WAS A DRAFT BEFORE");
                emailBean.setFolderKey(SENT_KEY);
                emailBean.setReceivedDate(null);
                //emailBean = createEmail(emailBean);
                LOG.debug("EMAILBEAN ID TO DELETE" + emailBean.getId());
                try {
                    //delete the email from the draft folder
                    emailDao.delete(emailBean.getId());
                    emailDao.createEmail(emailBean, SENT_KEY);
                    //emailDao.saveBlobToDisk(emailBean.getId());
                } catch (SQLException ex) {
                    errorAlert(resources.getString("invalidSqlMsg"));
                }

                emailAdded = true;
                LOG.debug("EMAIL HAS BEEN SENT");
            }
        }
        //if the email is added to the db properly then go to the sent folder 
        if (emailAdded) {
            setDefaultFields();
            treeLayoutController.setItemPosition(SENT_POSITION);
            tableLayoutController.displayTable(emailBean.getFolderKey());
        }
    }

    /**
     * this displays the image or files in the html editor
     *
     * @param file
     */
    private void displayImagesInHtml(File file) {
        StringBuilder sb = new StringBuilder();
        //get previous text
        sb.append(htmlEditor.getHtmlText());
        //append attachment
        sb.append("<img src=' ").append(file.toURI()).append("'/>");
        //set new text
        htmlEditor.setHtmlText(sb.toString());
    }

    /**
     * Error message popup dialog
     *
     * @param msg
     */
    private void errorAlert(String msg) {
        Alert dialog = new Alert(Alert.AlertType.ERROR);
        dialog.setTitle(resources.getString("sqlError"));
        dialog.setHeaderText(msg);
        dialog.setContentText("sqlError");
        dialog.showAndWait();
    }
    

    /**
     * an alert for when a file is not saved in the root
     */
    private void NoSuchFileAlert() {
        LOG.info("THE IMAGE MUST BE SAVED IN ROOT");
        Alert dialog = new Alert(Alert.AlertType.ERROR);
        dialog.setTitle(resources.getString("error"));
        dialog.setHeaderText(resources.getString("noFile"));
        dialog.setContentText(resources.getString("noFileMsg"));
        dialog.showAndWait();
    }

    /**
     * error message when no email is in the to, cc ,bcc
     */
    private void noEmailFoundAlert() {
        LOG.debug("NO EMAIL FOUND");
        Alert dialog = new Alert(Alert.AlertType.ERROR);
        dialog.setTitle(resources.getString("error"));
        dialog.setHeaderText(resources.getString("noEmail"));
        dialog.setContentText(resources.getString("noEmailMsg"));
        dialog.show();
    }

    /**
     * error message when no email is in the to, cc ,bcc
     */
    private void invalidEmailFormatAlert() {
        LOG.debug("INVALID EMAIL FORMAT");
        Alert dialog = new Alert(Alert.AlertType.ERROR);
        dialog.setTitle(resources.getString("error"));
        dialog.setHeaderText(resources.getString("invalidFormat"));

        Label label = new Label(resources.getString("invalidFormatMsg"));
        label.setWrapText(true);
        dialog.getDialogPane().setContent(label);
        dialog.show();
    }

}
