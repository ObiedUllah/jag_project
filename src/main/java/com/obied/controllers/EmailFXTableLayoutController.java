package com.obied.controllers;

import com.obied.beans.EmailBean;
import com.obied.beans.MailConfigBean;
import com.obied.business.SendAndReceive;
import com.obied.exception.InvalidEmailFormatException;
import com.obied.persistence.EmailDAO;
import java.net.URL;
import java.sql.SQLException;
import java.time.ZoneId;
import java.util.Objects;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.input.ClipboardContent;
import javafx.scene.input.Dragboard;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.TransferMode;
import javafx.scene.layout.BorderPane;
import jodd.mail.ReceivedEmail;
import org.slf4j.LoggerFactory;

/**
 * this controller stores the table in the upper right split pane
 *
 * it has a method that will call the emailDAOimpl and display the tables with
 * the emails in a specific folder
 *
 * @author Obied
 */
public class EmailFXTableLayoutController {

    private final static org.slf4j.Logger LOG = LoggerFactory.getLogger(EmailFXTableLayoutController.class);

    @FXML // ResourceBundle that was given to the FXMLLoader
    private ResourceBundle resources;

    @FXML // URL location of the FXML file that was given to the FXMLLoader
    private URL location;

    @FXML // fx:id="tableBorderPane"
    private BorderPane tableBorderPane; // Value injected by FXMLLoader

    @FXML // fx:id="tableView"
    private TableView<EmailBean> tableView; // Value injected by FXMLLoader

    @FXML // fx:id="fromColumn"
    private TableColumn<EmailBean, String> fromColumn; // Value injected by FXMLLoader

    @FXML // fx:id="subjectColumn"
    private TableColumn<EmailBean, String> subjectColumn; // Value injected by FXMLLoader

    @FXML // fx:id="dateColumn"
    private TableColumn<EmailBean, String> dateColumn; // Value injected by FXMLLoader

    @FXML // fx:id="searchTextFld"
    private TextField searchTextFld; // Value injected by FXMLLoader

    private MailConfigBean mailConfigBean;

    private EmailFXTreeLayoutController treeLayoutController;

    private EmailDAO emailDao;

    private static final int INBOX_KEY = 1;

    /**
     * this functionality is not necessary for the current project.
     *
     * @param event
     */
    @FXML
    void onClickSearch(MouseEvent event) {
        //TODO 
        //this functionality is not necessary for the current project.
    }

    /**
     * this allows a user to pick up
     *
     * @param event
     */
    @FXML
    void onDragDetected(MouseEvent event) {
        String selected = "" + tableView.getSelectionModel().getSelectedItem().getId();
        LOG.debug("Selected row ID: " + selected);
        if (Objects.nonNull(selected)) {
            Dragboard db = tableView.startDragAndDrop(TransferMode.ANY);
            ClipboardContent content = new ClipboardContent();
            content.putString(selected);
            db.setContent(content);
            event.consume();
        }
    }

    /**
     * this method receives all the not read messages and tuerns them to
     * emailbean to add them in the db
     *
     * @param event
     * @throws InvalidEmailFormatException
     */
    @FXML
    void onMouseClickedRefresh(MouseEvent event) throws InvalidEmailFormatException {
        LOG.debug("onMouseClickedRefresh CLICKED");
        //receive all the emails
        SendAndReceive SendAndReceive = new SendAndReceive(mailConfigBean);
        ReceivedEmail[] receivedEmail = SendAndReceive.receiveEmail(mailConfigBean);

        boolean hasReceived = false;

        //go through every received email and create a new email in the db
        for (ReceivedEmail email : receivedEmail) {
            LOG.debug("RECEIVING EMAIL");
            LOG.debug("FROM: " + email.from().getEmail());
            LOG.debug("RECEIVING SUBJECT: " + email.subject());

            //create an emailbean for each received email
            EmailBean emailBean = new EmailBean();
            emailBean.setFolderKey(INBOX_KEY);
            emailBean.setReceivedDate(email.sentDate().toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime());

            //convert the receivedemail to a regular email
            LOG.debug("received email attachments" + email.attachments().toString());
            emailBean.email = emailBean.convertToEmail(email);
            LOG.debug("converted email attachments: " + emailBean.email.attachments().toString());

            try {
                emailDao.createEmail(emailBean, INBOX_KEY);
            } catch (SQLException ex) {
                errorAlert(resources.getString("invalidSqlMsg"));
            }
            hasReceived = true;
        }

        //if a single email was received then go to the inbox folder
        if (hasReceived) {
            treeLayoutController.setItemPosition(INBOX_KEY);
            displayTable(INBOX_KEY);
        }

    }

    @FXML // This method is called by the FXMLLoader when initialization is complete
    void initialize() {
        assert tableBorderPane != null : "fx:id=\"tableBorderPane\" was not injected: check your FXML file 'EmailFXTableLayout.fxml'.";
        assert tableView != null : "fx:id=\"tableView\" was not injected: check your FXML file 'EmailFXTableLayout.fxml'.";
        assert fromColumn != null : "fx:id=\"fromColumn\" was not injected: check your FXML file 'EmailFXTableLayout.fxml'.";
        assert subjectColumn != null : "fx:id=\"subjectColumn\" was not injected: check your FXML file 'EmailFXTableLayout.fxml'.";
        assert dateColumn != null : "fx:id=\"dateColumn\" was not injected: check your FXML file 'EmailFXTableLayout.fxml'.";
        assert searchTextFld != null : "fx:id=\"searchTextFld\" was not injected: check your FXML file 'EmailFXTableLayout.fxml'.";

        //get the cell info 
        fromColumn.setCellValueFactory(cellData -> cellData.getValue().toEmailTableBean().getFromProperty());
        subjectColumn.setCellValueFactory(cellData -> cellData.getValue().toEmailTableBean().getSubjectProperty());
        dateColumn.setCellValueFactory(cellData -> cellData.getValue().toEmailTableBean().getDateProperty());

        //adjust columns
        adjustColumnWidth();
    }

    /**
     * sets the mailconfigbean
     *
     * @param mailConfigBean
     */
    public void setMailConfigBean(MailConfigBean mailConfigBean) {
        this.mailConfigBean = mailConfigBean;
    }

    /**
     * sets emailDao
     *
     * @param emailDao
     */
    public void setEmailDAO(EmailDAO emailDao) {
        this.emailDao = emailDao;
    }

    /**
     * set the tree layout controller
     *
     * @param treeLayoutController
     */
    public void setTreeLayoutController(EmailFXTreeLayoutController treeLayoutController) {
        this.treeLayoutController = treeLayoutController;
    }

    /**
     * adjust width of the columns depending on the width of the borderPane
     */
    private void adjustColumnWidth() {
        int amtOfColumns = 3;
        tableView.setPrefWidth(tableBorderPane.prefWidthProperty().get());
        fromColumn.prefWidthProperty().bind(tableBorderPane.widthProperty().divide(amtOfColumns));
        subjectColumn.prefWidthProperty().bind(tableBorderPane.widthProperty().divide(amtOfColumns));
        dateColumn.prefWidthProperty().bind(tableBorderPane.widthProperty().divide(amtOfColumns));
    }

    /**
     * this will get the data from the Table used by other controllers
     *
     * @return
     */
    public TableView<EmailBean> getEmailDataTable() {
        return this.tableView;
    }

    /**
     * this will display the table depending on the folder selected
     *
     * @param folderid
     */
    public void displayTable(int folderid) {
        //set it to empty or else it will double the values everytime 
        tableView.setItems(FXCollections.observableArrayList());
        try {
            tableView.setItems(FXCollections.observableArrayList(emailDao.findFolder(folderid)));
        } catch (SQLException ex) {
            errorAlert(resources.getString("sqlError"));
        }

        // if theres no emails change the table content to no email
        if (tableView.getItems().isEmpty()) {
            tableView.setPlaceholder(new Label("No Email messages"));
        }
    }

    /**
     * Error message popup dialog
     *
     * @param msg
     */
    private void errorAlert(String msg) {
        Alert dialog = new Alert(Alert.AlertType.ERROR);
        dialog.setTitle(msg);
        dialog.setHeaderText(msg);
        dialog.setContentText(msg);
        dialog.showAndWait();
    }

}
