package com.obied.controllers;

import com.obied.beans.MailConfigBean;
import com.obied.beans.propertiesmanager.PropertiesManager;
import com.obied.controllers.config.EmailFXMailConfigFormController;
import com.obied.jag_project.MainAppFX;
import com.obied.persistence.EmailDAO;
import com.obied.persistence.EmailDAOimpl;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.MenuItem;
import javafx.scene.image.Image;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.web.WebView;
import javafx.stage.Modality;
import javafx.stage.Stage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * controller that will set the the other layouts inside of it This allows us to
 * use the standalone containers with minimal changes.
 *
 * @author Obied
 */
public class RootLayoutController {

    private final static Logger LOG = LoggerFactory.getLogger(RootLayoutController.class);

    @FXML // ResourceBundle that was given to the FXMLLoader
    private ResourceBundle resources;

    @FXML // URL location of the FXML file that was given to the FXMLLoader
    private URL location;

    //@FXML // fx:id="rootBorderPane"
    private BorderPane rootBorderPane; // Value injected by FXMLLoader

    @FXML // fx:id="closeMenuItem"
    private MenuItem closeMenuItem; // Value injected by FXMLLoader

    @FXML // fx:id="settingsMenuItem"
    private MenuItem settingsMenuItem; // Value injected by FXMLLoader

    @FXML // fx:id="aboutMenuItem"
    private MenuItem aboutMenuItem; // Value injected by FXMLLoader

    @FXML // fx:id="leftSplit"
    private AnchorPane leftSplit; // Value injected by FXMLLoader

    @FXML // fx:id="upperRightSplit"
    private AnchorPane upperRightSplit; // Value injected by FXMLLoader

    @FXML // fx:id="lowerRightSplit"
    private AnchorPane lowerRightSplit; // Value injected by FXMLLoader

    private EmailDAO emailDao;

    private EmailFXHTMLLayoutController htmlLayoutController;
    private EmailFXTreeLayoutController treeLayoutController;
    private EmailFXTableLayoutController tableLayoutController;

    /**
     * closes the application
     *
     * @param event
     */
    @FXML
    void onClickClose(ActionEvent event) {
        Platform.exit();
    }

    /**
     * will initilaize the webview controller and display the stage containing
     * the html file
     *
     * @param event
     */
    @FXML
    void onClickInfo(ActionEvent event) {
        try {
            //create fxmlLoader
            FXMLLoader webViewLoader = new FXMLLoader();
            webViewLoader.setLocation(MainAppFX.class
                    .getResource("/fxml/AboutWebView.fxml"));

            //load fxml
            WebView root = webViewLoader.load();
            AboutWebViewController controller = webViewLoader.getController();

            //set the stage 
            Stage stage = new Stage();
            stage.setScene(new Scene(root));

            // Set the application icon using getResourceAsStream.
            stage.getIcons().add(new Image(MainAppFX.class.getResourceAsStream("/images/email.png")));
            //set the title
            stage.setTitle(ResourceBundle.getBundle("MessagesBundle").getString("about"));

            stage.initModality(Modality.WINDOW_MODAL);

            stage.showAndWait();
        } catch (IOException e) {
            LOG.debug("IOEXCEPTION CAUGHT");
        }

    }

    /**
     * create a config stage mail using the properties manager
     *
     * @param event
     */
    @FXML
    void onClickSettings(ActionEvent event) throws IOException {
        //create default values
        PropertiesManager propertiesManager = new PropertiesManager();
        MailConfigBean mailConfigBean = new MailConfigBean();
        propertiesManager.loadTextProperties(mailConfigBean, "", "mailconfig");

        //create fcmlLoader
        FXMLLoader configLoader = new FXMLLoader();
        configLoader.setResources(resources);
        configLoader.setLocation(MainAppFX.class
                .getResource("/fxml/EmailFXConfigFormLayout.fxml"));

        //load fxml
        Parent settingsRoot = configLoader.load();
        EmailFXMailConfigFormController emailFXMailConfigFormController = configLoader.getController();

        //setup properties
        emailFXMailConfigFormController.setUpProperties(propertiesManager, mailConfigBean);

        //create the stage to show application
        Stage configStage = new Stage();
        configStage.setScene(new Scene(settingsRoot));
        configStage.setTitle(ResourceBundle.getBundle("MessagesBundle").getString("titleConfig"));

        // Set the application icon using getResourceAsStream.
        configStage.getIcons().add(new Image(MainAppFX.class.getResourceAsStream("/images/email.png")));
        configStage.setResizable(false);
        configStage.initModality(Modality.WINDOW_MODAL);
        LOG.info("Mail Config Stage Show and Wait");

        configStage.showAndWait();
        initializeFields(mailConfigBean);
    }

    @FXML // This method is called by the FXMLLoader when initialization is complete
    void initialize() {

        assert rootBorderPane != null : "fx:id=\"rootBorderPane\" was not injected: check your FXML file 'RootLayout.fxml'.";
        assert leftSplit != null : "fx:id=\"leftSplit\" was not injected: check your FXML file 'RootLayout.fxml'.";
        assert upperRightSplit != null : "fx:id=\"upperRightSplit\" was not injected: check your FXML file 'RootLayout.fxml'.";
        assert lowerRightSplit != null : "fx:id=\"lowerRightSplit\" was not injected: check your FXML file 'RootLayout.fxml'.";
        assert rootBorderPane != null : "fx:id=\"rootBorderPane\" was not injected: check your FXML file 'RootLayout.fxml'.";
        assert leftSplit != null : "fx:id=\"leftSplit\" was not injected: check your FXML file 'RootLayout.fxml'.";
        assert upperRightSplit != null : "fx:id=\"upperRightSplit\" was not injected: check your FXML file 'RootLayout.fxml'.";
        assert lowerRightSplit != null : "fx:id=\"lowerRightSplit\" was not injected: check your FXML file 'RootLayout.fxml'.";

    }

    /**
     * sets the email dao and also initializes the different split panes.
     *
     * @param mailConfigBean
     */
    public void initializeFields(MailConfigBean mailConfigBean) {
        this.emailDao = new EmailDAOimpl(mailConfigBean); //nothing to be displayed in the root layout since the emaildao will be null
        
        //initialize the different split panes
        initLeftSplit();
        initUpperRightSplit();
        initLowerRightSplit();
        
        //setting the values
        setTableControllerToTree();
        setHtmlControllertoHtml();
        
        //initialize the mailconfigbean inside the htmlLayoutController and the tree
        htmlLayoutController.setMailConfigBean(mailConfigBean);
        htmlLayoutController.setTreeLayoutController(treeLayoutController);
        
        //initialize the htmllayout for the tree
        treeLayoutController.setHTMLLayoutController(htmlLayoutController);
        
        //initialize congibBean for table to use to refresh
        tableLayoutController.setMailConfigBean(mailConfigBean);
        tableLayoutController.setTreeLayoutController(treeLayoutController);
        
        //this will display the initial content in the gui
        treeLayoutController.displayTree();
        htmlLayoutController.displayForm();
        htmlLayoutController.disableFieldsForTree();

    }

    /**
     * sets the tables controller by reference to the tree controller
     */
    private void setTableControllerToTree() {
        treeLayoutController.setTableController(tableLayoutController);
    }

    /**
     * sets the table Controller by reference to the tree controller
     */
    private void setHtmlControllertoHtml() {
        htmlLayoutController.setTableController(tableLayoutController);
    }

    /**
     * initialized the tree view in the left split pane
     */
    private void initLeftSplit() {
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setResources(resources);

            loader.setLocation(RootLayoutController.class
                    .getResource("/fxml/EmailFXTreeLayout.fxml"));
            BorderPane treeView = (BorderPane) loader.load();

            // Give the controller the data object.
            treeLayoutController = loader.getController();
            treeLayoutController.setEmailDAO(emailDao);

            //bind the size to the parent
            treeView.prefWidthProperty().bind(leftSplit.widthProperty());
            treeView.prefHeightProperty().bind(leftSplit.heightProperty());

            leftSplit.getChildren().add(treeView);
        } catch (IOException ex) {
            LOG.error("initLeftSplit error", ex);
            errorAlert("initLeftSplit()");
            Platform.exit();
        }
    }

    /**
     * initialized the table view in the right upper split pane
     */
    private void initUpperRightSplit() {
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setResources(resources);

            loader.setLocation(RootLayoutController.class
                    .getResource("/fxml/EmailFXTableLayout.fxml"));
            BorderPane tableView = (BorderPane) loader.load();

            // Give the controller the data object.
            tableLayoutController = loader.getController();
            tableLayoutController.setEmailDAO(emailDao);

            //bind the size to the parent
            tableView.prefWidthProperty().bind(upperRightSplit.widthProperty());
            tableView.prefHeightProperty().bind(upperRightSplit.heightProperty());

            upperRightSplit.getChildren().add(tableView);
        } catch (IOException ex) {
            LOG.error("initUpperRightSplit error", ex);
            errorAlert("initUpperRightSplit()");
            Platform.exit();
        }
    }

    /**
     * initialized the tree view in the left split pane
     */
    private void initLowerRightSplit() {
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setResources(resources);

            loader.setLocation(RootLayoutController.class
                    .getResource("/fxml/EmailFXHTMLLayout.fxml"));
            BorderPane htmlView = (BorderPane) loader.load();

            // Give the controller the data object.
            htmlLayoutController = loader.getController();
            htmlLayoutController.setEmailDao(emailDao);

            //bind the size to the parent
            htmlView.prefWidthProperty().bind(lowerRightSplit.widthProperty());
            htmlView.prefHeightProperty().bind(lowerRightSplit.heightProperty());

            lowerRightSplit.getChildren().add(htmlView);
        } catch (IOException ex) {
            LOG.error("initLowerRightSplit error", ex);
            errorAlert("initLowerRightSplit()");
            Platform.exit();
        }
    }

    /**
     * Error message popup dialog
     *
     * @param msg
     */
    private void errorAlert(String msg) {
        Alert dialog = new Alert(Alert.AlertType.ERROR);
        dialog.setTitle(resources.getString("sqlError"));
        dialog.setHeaderText(msg);
        dialog.setContentText("for testing, check the Javadocs comment in PropertiesManager");
        dialog.showAndWait();
    }

}
