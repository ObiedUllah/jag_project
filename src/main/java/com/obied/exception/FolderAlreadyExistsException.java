/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.obied.exception;

/**
 *
 * @author Obied
 */
public class FolderAlreadyExistsException extends Exception {

    /**
     * Creates a new instance of <code>FolderAlreadyExists</code> without detail
     * message.
     */
    public FolderAlreadyExistsException() {
    }

    /**
     * Constructs an instance of <code>FolderAlreadyExists</code> with the
     * specified detail message.
     *
     * @param msg the detail message.
     */
    public FolderAlreadyExistsException(String msg) {
        super(msg);
    }
}
