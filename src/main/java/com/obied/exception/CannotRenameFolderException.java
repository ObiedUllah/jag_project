/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.obied.exception;

/**
 *
 * @author Obied
 */
public class CannotRenameFolderException extends Exception {

    /**
     * Creates a new instance of <code>CannotRenameFolder</code> without detail
     * message.
     */
    public CannotRenameFolderException() {
    }

    /**
     * Constructs an instance of <code>CannotRenameFolder</code> with the
     * specified detail message.
     *
     * @param msg the detail message.
     */
    public CannotRenameFolderException(String msg) {
        super(msg);
    }
}
