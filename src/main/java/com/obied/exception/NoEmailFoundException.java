package com.obied.exception;

/**
 * throws exception when it finds no email address to be sent to
 * @author Obied
 */
public class NoEmailFoundException extends Exception {

    /**
     * Creates a new instance of <code>NoEmailFoundException</code> without
     * detail message.
     */
    public NoEmailFoundException() {
    }

    /**
     * Constructs an instance of <code>NoEmailFoundException</code> with the
     * specified detail message.
     *
     * @param msg the detail message.
     */
    public NoEmailFoundException(String msg) {
        super(msg);
    }
}
