/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.obied.exception;

/**
 *
 * @author Obied
 */
public class InvalidDeleteFolderException extends Exception {

    /**
     * Creates a new instance of <code>InvvalidDeleteFolder</code> without
     * detail message.
     */
    public InvalidDeleteFolderException() {
    }

    /**
     * Constructs an instance of <code>InvvalidDeleteFolder</code> with the
     * specified detail message.
     *
     * @param msg the detail message.
     */
    public InvalidDeleteFolderException(String msg) {
        super(msg);
    }
}
