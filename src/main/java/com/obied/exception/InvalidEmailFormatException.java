package com.obied.exception;

/**
 * Exception is thrown if the email address format is wrong
 * @author Obied
 */
public class InvalidEmailFormatException extends Exception {

    /**
     * Creates a new instance of <code>InvalidEmailFormatException</code>
     * without detail message.
     */
    public InvalidEmailFormatException() {
    }

    /**
     * Constructs an instance of <code>InvalidEmailFormatException</code> with
     * the specified detail message.
     *
     * @param msg the detail message.
     */
    public InvalidEmailFormatException(String msg) {
        super(msg);
    }
}
