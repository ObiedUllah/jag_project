/**
 * This creates the Address table and inserts test data
 * The address table stores an id with an email address which is unique 
 * Author:  Obied
 * Created: Oct. 1, 2020
 */

set FOREIGN_KEY_CHECKS = 0;
drop table if exists Address;
set FOREIGN_KEY_CHECKS = 1;

create table Address(
    EmailAddressId INT auto_increment,
    EmailAddress VARCHAR(320) not null,
    primary key(EmailAddressId)
);

insert into Address(EmailAddress)
values('aacctestemail1@gmail.com'),
('bacctestemail1@gmail.com'),
('cacctestemail1@gmail.com'),
('dacctestemail1@gmail.com'),
('eacctestemail1@gmail.com');



/**
 * this creates the folder table and inserts test data 
 * folder naems have to be unique 
 * Author:  Obied
 * Created: Oct. 1, 2020
 */

set FOREIGN_KEY_CHECKS = 0;
drop table if exists Folder;
set FOREIGN_KEY_CHECKS = 1;

create table Folder(
    FolderId INT auto_increment,
    FolderName VARCHAR(255) not null default '' unique,
    primary key (FolderId)
);

insert into Folder(FolderName)
values('Inbox'),
('Sent'),
('Draft'),
('XYZ');



/**
 * This creates the email table and inserts test values
 * the email has a folderid which connects to the Folder table through the id
 * each record has the content of a email
 * each record has a sendDate and received date which relates to the folder the email is in 
 * Author: Obied
 * Created: Oct. 1, 2020
 */

set FOREIGN_KEY_CHECKS = 0;
drop table if exists Email;
set FOREIGN_KEY_CHECKS = 1;

create table Email(
    EmailId INT auto_increment,
    FolderId INT default 1,
    --sender's email address not connected to EmailToAddress
    FromAddress VARCHAR(320) not null , 
    Subject VARCHAR(255) not null,
    TextMessage TEXT not null,
    HtmlMessage TEXT not null,
    --if send null then it was a received email
    SendDate TIMESTAMP, 
    --if received null then it was a sent email && if both null then draft 
    ReceivedDate TIMESTAMP, 
    constraint FK_FolderIdToFolder foreign key (FolderId) references Folder(FolderId),
    primary key(EmailId)
);

-- The folderIds, senddates, receivedDates must make sense together  
insert into Email( FolderId, FromAddress, Subject, TextMessage, HtmlMessage, SendDate, ReceivedDate)
--emails that have been received by a user. they should be able to see when it was sent to them
values(1,'bacctestemail1@gmail.com', 'One piece', 'Luffy', '<p> hello 1 </p>', '2020-10-02 20:02:40', '2020-10-02 20:02:45'),
(1,'bacctestemail1@gmail.com', 'Naruto', 'Sasuke uchiha', '<p> hello 2 </p>', '2020-10-01 20:02:40', '2020-10-01 20:02:45'),
--emails that have been received by a user. they should be able to see when it was sent to them, in the xyz folder
(4,'cacctestemail1@gmail.com', 'Bleach', 'Ichigo uchiha', '<p> hello 3</p>', '2020-09-30 20:02:40', '2020-09-30 20:02:45'),
--Drafts
(3,'aacctestemail1@gmail.com', 'Two piece', 'monkey d luffy', '<p> bye 4</p>', null, null),
(3,'aacctestemail1@gmail.com', 'Dragon ball', 'Goku', '<p> bye 5</p>', null, null), 
--Sent emails by a user
(2,'aacctestemail1@gmail.com', 'Fairy tail', 'Laxus', '<p>welcome 6</p>', '2020-09-28 20:02:40', null ),
(2,'aacctestemail1@gmail.com', 'Hunter x hunter', 'Kurapika', '<p>welcome 7</p>', '2020-09-28 20:02:40', null );



/**
 * This creates the Attachment table and inserts test data into it
 * The attachment table holds all the attchments and the content related to them
 * also includes the emailId in which they belong 
 * Author:  Obied
 * Created: Oct. 1, 2020
 */

set FOREIGN_KEY_CHECKS = 0;
drop table if exists Attachment;
set FOREIGN_KEY_CHECKS = 1;

create table Attachment(
    AttachmentId INT auto_increment,
    EmailId INT not null,  
    FileName VARCHAR(128) not null default '',
    -- if empty or null = regular else = embedded
    ContentId VARCHAR(255),
    --stored as a mediumblob and retrieved as a byte array  
    Attachment MEDIUMBLOB, 
    primary key(AttachmentId),
    constraint FK_EmailIdToEmail foreign key (EmailId) references Email(EmailId)
);


insert into Attachment(FileName, EmailId, ContentId, Attachment)
--embedded
values('WindsorKen180-TEST.jpg',1,'embedded',null), 
--regular
('FreeFall-TEST.jpg', 2, null, null), 
--regular 
('WindsorKen180-TEST.jpg', 3, '', null),
--embedded
('FreeFall-TEST.jpg',4, 'embedded', null),
--multiple attachments in one email
('FreeFall-TEST.jpg', 5, 'embedded', null),
('FreeFall-TEST.jpg', 5, 'embedded', null),
('FreeFall-TEST.jpg', 5, '', null),
('FreeFall-TEST.jpg', 5, '', null);



/**
 * This  creates the EmailToAddress table ande inserts test data
 * this is a bridging table for Email and Address
 * includes a category which is the field
 * Author:  Obied
 * Created: Oct. 1, 2020
 */

set FOREIGN_KEY_CHECKS = 0;
drop table if exists EmailToAddress;
set FOREIGN_KEY_CHECKS = 1;

create table EmailToAddress(
    --email being sent
    EmailId INT not null, 
    --receiver's email address
    RecipientEmailAddressId INT not null, 
    -- TO, CC, BCC
    Category VARCHAR(3) not null , 
    constraint FK_EmailIdToAddressToEmail foreign key (EmailId) references Email(EmailId),
    constraint FK_RecipientEmailAddressIdToAddress foreign key(RecipientEmailAddressId) references Address(EmailAddressId)
);

insert into EmailToAddress(EmailId, RecipientEmailAddressId, Category)
--received email by three different method to same user
values(1 , 3, 'TO'), 
(1, 3, 'CC'), 
(1, 3, 'BCC'),
--recived email by multiple TO
(2, 4, 'TO' ),
(2, 5, 'TO'),
--recieved email by multiple CC and BCC
(3, 4, 'CC'),
(3, 5, 'CC'),
(3, 4, 'BCC'),
(3, 5, 'BCC'),
--Drafts, these have not been sent yet. added random values
(4, 3, 'TO'),
(5, 4, 'CC'),
(5, 5, 'BCC'),
--sent email to people
(6, 5, 'TO'),
(7, 5, 'CC'),
(7, 5, 'BCC');
