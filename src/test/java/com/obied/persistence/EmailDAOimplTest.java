package com.obied.persistence;

import com.obied.beans.EmailBean;
import com.obied.beans.MailConfigBean;
import com.obied.exception.CannotRenameFolderException;
import com.obied.exception.FolderAlreadyExistsException;
import com.obied.exception.InvalidDeleteFolderException;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;
import java.io.StringReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;
import jodd.mail.EmailAttachment;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Obied
 */
public class EmailDAOimplTest {

    @Rule
    public ExpectedException thrown = ExpectedException.none();
    private final static Logger LOG = LoggerFactory.getLogger(EmailDAOimplTest.class);

    private MailConfigBean mailConfigBean;
    private EmailDAO emailDAOImpl;
    private String mySqlUrl;

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @Before
    public void init() {
        this.mailConfigBean = new MailConfigBean("", "", "", "", "", "", "", "jdbc:mysql://localhost", "EmailClient", "3306", "obied", "obiedStandard");
        this.emailDAOImpl = new EmailDAOimpl(mailConfigBean);
        this.mySqlUrl = mailConfigBean.getMysqlURL() + ":" + mailConfigBean.getMysqlPort() + "/" + mailConfigBean.getMysqlDatabase();
        LOG.debug(mySqlUrl);
        LOG.info("@Before seeding");
        //seedDatabase("createDb.sql");
        seedDatabase("createTables.sql");
    }

    @After
    public void tearDown() throws Exception {
    }

    /**
     * This routine recreates the database before every test.This makes sure
     * that a destructive test will not interfere with any other test. Does not
     * support stored procedures.
     *
     * This routine is courtesy of Bartosz Majsak, an Arquillian developer at
     * JBoss
     *
     * @param fileName
     */
    public void seedDatabase(String fileName) {

        final String seedDataScript = loadAsString(fileName);
        try ( Connection connection = DriverManager.getConnection(this.mySqlUrl, mailConfigBean.getMysqlUser(), mailConfigBean.getMysqlPassword());) {
            for (String statement : splitStatements(new StringReader(seedDataScript), ";")) {
                connection.prepareStatement(statement).execute();
            }
        } catch (SQLException e) {
            throw new RuntimeException("Failed seeding database", e);
        }
    }

    /**
     * The following three methods support the seedDatabse method
     */
    private String loadAsString(final String path) {
        try ( InputStream inputStream = Thread.currentThread().getContextClassLoader().getResourceAsStream(path);  Scanner scanner = new Scanner(inputStream)) {
            return scanner.useDelimiter("\\A").next();
        } catch (IOException e) {
            throw new RuntimeException("Unable to close input stream.", e);
        }
    }

    private List<String> splitStatements(Reader reader, String statementDelimiter) {
        final BufferedReader bufferedReader = new BufferedReader(reader);
        final StringBuilder sqlStatement = new StringBuilder();
        final List<String> statements = new LinkedList<>();
        try {
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                line = line.trim();
                if (line.isEmpty() || isComment(line)) {
                    continue;
                }
                sqlStatement.append(line);
                if (line.endsWith(statementDelimiter)) {
                    statements.add(sqlStatement.toString());
                    sqlStatement.setLength(0);
                }
            }
            return statements;
        } catch (IOException e) {
            throw new RuntimeException("Failed parsing sql", e);
        }
    }

    private boolean isComment(final String line) {
        return line.startsWith("--") || line.startsWith("//") || line.startsWith("/*") || line.startsWith("*");
    }


    /*Test cases **************************************************************************************************/
 /*Create ************************************************************************************************/
    /**
     * check if a new record is created in the email table
     *
     * @throws SQLException
     * @throws java.text.ParseException
     */
    @Test
    public void createEmailCheckIfRecordCreated() throws SQLException, ParseException {
        int result = emailDAOImpl.createEmail(creatingExampleBean(), 2);
        assertEquals("createEmailCheckIfRecordCreated: ", 1, result);
    }

    /**
     * check if a record is created is the folder table
     *
     * @throws SQLException
     * @throws com.obied.exception.FolderAlreadyExistsException
     * @throws java.text.ParseException
     */
    @Test
    public void createFolderCheckIfRecordCreated() throws SQLException, FolderAlreadyExistsException, ParseException {
        int result = emailDAOImpl.createFolder("Trash");
        assertEquals("createFolderCheckIfRecordCreated: ", 1, result);
    }

    /**
     * this method checks if a folder is created and that emails can be added to
     * it to make sure it exists and usable.
     *
     * @throws SQLException
     * @throws com.obied.exception.FolderAlreadyExistsException
     * @throws java.text.ParseException
     */
    @Test
    public void createFolderCheckContent() throws SQLException, FolderAlreadyExistsException, ParseException {
        int result = emailDAOImpl.createFolder("Trash");
        //the emailBean will have a single value -> an email that has been received
        ArrayList<EmailBean> email = (ArrayList<EmailBean>) emailDAOImpl.findFields("one piece");
        //setting the folderkey of the email to 4 to put it in the new folder
        email.get(0).setFolderKey(4);
        //add the email to the folder
        emailDAOImpl.updateFolder(email.get(0));
        assertEquals("createFolderCheckContent: ", 1, email.size());
    }

    @Test
    public void createAlreadyExistingFolder() throws SQLException, FolderAlreadyExistsException, ParseException {
        thrown.expect(FolderAlreadyExistsException.class);
        emailDAOImpl.createFolder("Inbox");
    }

    /**
     * This helper method simply creates a bean with a lot of info. The id of
     * this bean
     */
    private EmailBean creatingExampleBean() throws ParseException {
        LOG.debug("creating example bean");
        EmailBean emailBean = new EmailBean();
        File file = new File("WindsorKen180-TEST.jpg");
        File file2 = new File("FreeFall-TEST.jpg");
        LOG.debug("file added");
        
        emailBean.setId(8);
        emailBean.setFolderKey(2);
        emailBean.setReceivedDate(null);
        LOG.debug("emailBean set content");
        emailBean.email.from("aacctestemail1@gmail.com")
                .subject("new subject")
                .textMessage("new Text Message")
                .htmlMessage("<p>new html message 8</p>")
                .to("bacctestemail1@gmail.com")
                .to("cacctestemail1@gmail.com")
                .cc("bacctestemail1@gmail.com")
                .cc("cacctestemail1@gmail.com")
                .bcc("bacctestemail1@gmail.com")
                .bcc("cacctestemail1@gmail.com")
                .attachment(EmailAttachment.with().content(file))
                .embeddedAttachment(EmailAttachment.with().content(file2).name(file2.getName()));
        LOG.debug("emailbean created");
        return emailBean;
    }

    /*Find ****************************************************************************************************/
    /**
     * checks if the amount of total records is correct
     *
     * @throws SQLException
     * @throws java.text.ParseException
     */
    @Test
    public void findAllLength() throws SQLException, ParseException {
        ArrayList<EmailBean> list = (ArrayList<EmailBean>) emailDAOImpl.findAll();
        assertEquals("findAllLength: ", 7, list.size());
    }

    /**
     * check if the subjects returned by the findall methods are valid for this
     * method i decided to check them all
     *
     * @throws SQLException
     * @throws java.text.ParseException
     */
    @Test
    public void findAllSubject() throws SQLException, ParseException {
        ArrayList<EmailBean> list = (ArrayList<EmailBean>) emailDAOImpl.findAll();
        //list with expected subject names
        ArrayList<String> namesList = new ArrayList<>();
        namesList.add("One piece");
        namesList.add("Naruto");
        namesList.add("Bleach");
        namesList.add("Two piece");
        namesList.add("Dragon ball");
        namesList.add("Fairy tail");
        namesList.add("Hunter x hunter");
        int subjectCount = 0;
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i).email.subject().contentEquals(namesList.get(i))) {
                subjectCount++;
            }
        }
        assertEquals(7, subjectCount);
    }

    /**
     * check if id of the email is searched properly
     *
     * @throws SQLException
     * @throws java.text.ParseException
     */
    @Test
    public void findById() throws SQLException, ParseException {
        EmailBean emailBean = emailDAOImpl.findById(1);
        assertEquals("findById", 1, emailBean.getId());
    }

    /**
     * check if the text messages returned by the findall methods are valid for
     * this method decided to check 2
     *
     * @throws SQLException
     * @throws java.text.ParseException
     */
    @Test
    public void findAllTextMsg() throws SQLException, ParseException {
        ArrayList<EmailBean> list = (ArrayList<EmailBean>) emailDAOImpl.findAll();
        //list with expected subject names
        ArrayList<String> textList = new ArrayList<>();
        textList.add("Luffy");
        textList.add("Sasuke uchiha");
        int textCount = 0;
        for (int i = 0; i < textList.size(); i++) {
            if (list.get(i).email.messages().get(0).getContent().contentEquals(textList.get(i))) {
                textCount++;
            }
        }
        assertEquals(2, textCount);
    }

    /**
     * gets all the records that match a field this one tests email address
     *
     * @throws SQLException
     * @throws java.text.ParseException
     */
    @Test
    public void findFieldsEmailAddress() throws SQLException, ParseException {
        LOG.info("TEST : findFieldsEmailAddress");
        ArrayList<EmailBean> list = (ArrayList<EmailBean>) emailDAOImpl.findFields("aacctest");
        assertEquals(4, list.size());
    }

    /**
     * gets all the records that match a field this one tests the subject
     *
     * @throws SQLException
     * @throws java.text.ParseException
     */
    @Test
    public void findFieldsSubject() throws SQLException, ParseException {
        ArrayList<EmailBean> list = (ArrayList<EmailBean>) emailDAOImpl.findFields("Piece");
        assertEquals("findFieldsSubject: ", 2, list.size());
    }

    /**
     * gets all the records that match a field this one tests the text message
     *
     * @throws SQLException
     * @throws java.text.ParseException
     */
    @Test
    public void findFieldsTextMsg() throws SQLException, ParseException {
        ArrayList<EmailBean> list = (ArrayList<EmailBean>) emailDAOImpl.findFields("iha");
        assertEquals("findFieldsTextMsg: ", 2, list.size());
    }

    /**
     * gets all the records that match a field this one tests the html message
     *
     * @throws SQLException
     * @throws java.text.ParseException
     */
    @Test
    public void findFieldsHtmlMsg() throws SQLException, ParseException {
        ArrayList<EmailBean> list = (ArrayList<EmailBean>) emailDAOImpl.findFields("WELCOME");
        assertEquals("findFieldsHtmlMsg: ", 2, list.size());
    }

    /**
     * gets all the records in a specific folder, in this case inbox
     *
     * @throws SQLException
     * @throws java.text.ParseException
     */
    @Test
    public void findFolderInbox() throws SQLException, ParseException {
        ArrayList<EmailBean> list = (ArrayList<EmailBean>) emailDAOImpl.findFolder(1);
        assertEquals("findFolderInbox: ", 2, list.size());
    }

    /**
     * gets all the records in a specific folder, in this case sent
     *
     * @throws SQLException
     * @throws java.text.ParseException
     */
    @Test
    public void findFolderSent() throws SQLException, ParseException {
        ArrayList<EmailBean> list = (ArrayList<EmailBean>) emailDAOImpl.findFolder(2);
        assertEquals("findFolderSent: ", 2, list.size());
    }

    /**
     * gets all the records in a specific folder, in this case draft
     *
     * @throws SQLException
     * @throws java.text.ParseException
     */
    @Test
    public void findFolderDraft() throws SQLException, ParseException {
        ArrayList<EmailBean> list = (ArrayList<EmailBean>) emailDAOImpl.findFolder(3);
        assertEquals("findFolderDraft: ", 2, list.size());
    }

    /**
     * Test if all attachments are properly added to an attachment.this test
     * will be made with email having id 5 which has 2 embedded attachments and
     * 2 regular attachments.
     *
     * @throws SQLException
     * @throws java.text.ParseException
     */
    @Test
    public void findAttachmentsLength() throws SQLException, ParseException {
        //this emailBean will have a size of one
        EmailBean emailBean = emailDAOImpl.findById(5);//i put the emailid inside the html for easier finding
        int attachmentsCount = emailBean.email.attachments().size();
        assertEquals(6, attachmentsCount);
    }

    /**
     * this will test out if the regular attachments are properly added to the
     * bean.this test will be made with email having id 5 which has 2 regular
     * attachments. to get email with id 5, we just find a field specific to
     * that email
     *
     * @throws SQLException
     * @throws java.text.ParseException
     */
    @Test
    public void findRegularAttachmentsCheck() throws SQLException, ParseException {
        //this emailBean will have a size of one
        EmailBean emailBean = emailDAOImpl.findById(5);

        int attachmentsCount = 0;
        //go through all the attachments in the email with id 5
        for (EmailAttachment item : emailBean.email.attachments()) {
            //check item is regular
            if (!item.isEmbedded()) {
                attachmentsCount++;
            }
        }
        assertEquals("findRegularAttachmentsCheck: ", 2, attachmentsCount);
    }

    /**
     * this will test out if the embedded attachments are properly added to the
     * bean.this test will be made with email having id 5 which has 2 embedded
     * attachments.
     *
     * @throws SQLException
     * @throws java.text.ParseException
     */
    @Test
    public void findEmbeddedAttachmentsCheck() throws SQLException, ParseException {
        EmailBean emailBean = emailDAOImpl.findById(5);

        int attachmentsCount = 0;
        //go through all the attachments in the email with id 5
        for (EmailAttachment item : emailBean.email.attachments()) {
            //check item is embedded
            if (item.isEmbedded()) {
                attachmentsCount++;
            }
        }
        assertEquals("findEmbeddedAttachmentsCheck: ", 4, attachmentsCount);
    }

    /**
     * this will check if the TO fields are properly added to the bean this test
     * will be made with email having id 2 which has 2 'TO'
     *
     * @throws SQLException
     * @throws java.text.ParseException
     */
    @Test
    public void findToForReceived() throws SQLException, ParseException {
        EmailBean emailBean = emailDAOImpl.findById(2);
        int toCount = emailBean.email.to().length;
        assertEquals("findToForReceived: ", 2, toCount);
    }

    /**
     * this will check if the CC fields are properly added to the bean.this test
     * will be made with email having id 3 which has 2 'CC'
     *
     * @throws SQLException
     * @throws java.text.ParseException
     */
    @Test
    public void findCCForReceived() throws SQLException, ParseException {
        EmailBean emailBean = emailDAOImpl.findById(3);
        int ccCount = emailBean.email.cc().length;
        assertEquals("findCCForReceived: ", 2, ccCount);
    }

    /**
     * this will check if the BCC fields are not added to the bean since this
     * email has been received.this test will be made with email having id 3
     * which has 2 'BCC'. There should be no bcc fields.
     *
     * @throws SQLException
     * @throws java.text.ParseException
     */
    @Test
    public void findNoBCCForReceived() throws SQLException, ParseException {
        EmailBean emailBean = emailDAOImpl.findById(3);
        int bccCount = emailBean.email.bcc().length;
        assertEquals("findNoBCCForReceived", 0, bccCount);
    }

    /**
     * this checks if the user can see the bcc field in his draft which he
     * should because he is sending the email and adding the field
     *
     * @throws SQLException
     * @throws java.text.ParseException
     */
    @Test
    public void findBCCForDraft() throws SQLException, ParseException {
        EmailBean emailBean = emailDAOImpl.findById(5);
        int bccCount = emailBean.email.bcc().length;
        assertEquals("findBCCForDraft", 1, bccCount);
    }

    /**
     * this checks if the user can see the bcc field in his sent which he should
     * because he is the one who sent the email and added the field
     *
     * @throws SQLException
     * @throws java.text.ParseException
     */
    @Test
    public void findBCCForSent() throws SQLException, ParseException {
        //this emailBean will have a size of one
        ArrayList<EmailBean> list = (ArrayList<EmailBean>) emailDAOImpl.findFields("7");
        int bccCount = list.get(0).email.bcc().length;
        assertEquals("findBCCforSent", 1, bccCount);
    }

    /*UPDATE **********************************************************************************************/
    /**
     * update the location of the email.this checks if the record was updated in
     * the table
     *
     * @throws SQLException
     * @throws java.text.ParseException
     */
    @Test
    public void updateFolderCheckRecordUpdated() throws SQLException, ParseException {
        EmailBean emailBean = creatingExampleBean();
        //create email
        emailDAOImpl.createEmail(emailBean, 2);
        //changing folder
        emailBean.setFolderKey(1);
        int result = emailDAOImpl.updateFolder(emailBean);
        assertEquals("updateFolderCheckRecordUpdated: ", 1, result);
    }

    /**
     * checks if email changed folders.looks if the amount of emails in the new
     * folder increased by one
     *
     * @throws SQLException
     * @throws java.text.ParseException
     */
    @Test
    public void updateFoldercheckLength() throws SQLException, ParseException {
        EmailBean emailBean = emailDAOImpl.findById(2);
        //changing folder
        emailBean.setFolderKey(1);
        int result = emailDAOImpl.updateFolder(emailBean);
        ArrayList<EmailBean> length = (ArrayList<EmailBean>) emailDAOImpl.findFolder(1);
        assertEquals("updateFolderCheckRecordUpdated: ", 2, length.size());
    }


    /**
     * changes the name of the folder and finds all the emails in the new folder
     * name to makes sure the length is good
     *
     * @throws SQLException
     * @throws CannotRenameFolderException
     * @throws java.text.ParseException
     * @throws com.obied.exception.FolderAlreadyExistsException
     */
    @Test
    public void renameFolder() throws SQLException, CannotRenameFolderException, ParseException, FolderAlreadyExistsException {
        int result = emailDAOImpl.renameFolder("XYZ", "new");
        ArrayList<EmailBean> list = (ArrayList<EmailBean>) emailDAOImpl.findFolder(4);
        assertEquals("renameFolder", list.size(), 1);
    }

    /**
     * makes sure you cannot change folder name of inbox sent draft
     *
     * @throws SQLException
     * @throws CannotRenameFolderException
     * @throws com.obied.exception.FolderAlreadyExistsException
     */
    @Test
    public void renameFolderInbox() throws SQLException, CannotRenameFolderException, FolderAlreadyExistsException {
        thrown.expect(CannotRenameFolderException.class);
        int result = emailDAOImpl.renameFolder("Inbox", "hello");
    }

    /*DELETE *************************************************************************************************/
    /**
     * checking email deleted
     *
     * @throws SQLException
     */
    @Test
    public void deleteCheckRecordDeleted() throws SQLException {
        int result = emailDAOImpl.delete(7);
        assertEquals("deleteCheckRecordDeleted", 1, result);
    }

    /**
     * checking the amount of records in the database is one less
     *
     * @throws SQLException
     * @throws java.text.ParseException
     */
    @Test
    public void deleteCheckLength() throws SQLException, ParseException {
        int result = emailDAOImpl.delete(7);
        ArrayList<EmailBean> list = (ArrayList<EmailBean>) emailDAOImpl.findAll();
        assertEquals("deleteCheckLength", 6, list.size());
    }

    /**
     * this deletes a folder and all the emails attached to it
     *
     * @throws SQLException
     * @throws com.obied.exception.InvalidDeleteFolderException
     * @throws java.text.ParseException
     */
    @Test
    public void deleteFolder() throws SQLException, InvalidDeleteFolderException, ParseException {
        int result = emailDAOImpl.deleteFolder("DeleteTest");
        ArrayList<EmailBean> list = (ArrayList<EmailBean>) emailDAOImpl.findFolder(4);
        assertEquals("deleteFolder: ", 1, list.size());
    }

    /**
     * this will try to delete the inbox folder and fail
     *
     * @throws SQLException
     * @throws InvalidDeleteFolderException
     */
    @Test
    public void deleteInboxFolder() throws SQLException, InvalidDeleteFolderException {
        thrown.expect(InvalidDeleteFolderException.class);
        int result = emailDAOImpl.deleteFolder("Inbox");
    }
}
