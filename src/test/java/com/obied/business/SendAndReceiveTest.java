package com.obied.business;

import com.obied.beans.MailConfigBean;
import com.obied.exception.InvalidEmailFormatException;
import com.obied.exception.NoEmailFoundException;
import java.io.File;
import java.io.FileNotFoundException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import jodd.mail.Email;
import jodd.mail.MailException;
import jodd.mail.ReceivedEmail;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Rule;
import org.junit.rules.ExpectedException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Obied
 */
public class SendAndReceiveTest {

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    private final static Logger LOG = LoggerFactory.getLogger(SendAndReceiveTest.class);
    private final int secondsToSleep = 3;

    private String smtpServerName;
    private String imapServerName;
    private String emailSend;
    private String emailSendPwd;
    private String emailReceive;
    private String emailReceivePwd;
    private String emailReceive2;
    private String emailReceive2Pwd;
    private String emailReceive3;
    private String emailReceive3Pwd;

    private MailConfigBean mailConfigBean;
    private SendAndReceive sendAndReceive;

    private ArrayList<String> toList;
    private ArrayList<String> ccList;
    private ArrayList<String> bccList;
    private String subject;
    private String textMsg;
    private String htmlMsg;
    private ArrayList<File> regularAttachments;
    private ArrayList<File> embeddedAttachments;

    @Before
    public void init() {
        smtpServerName = "smtp.gmail.com";
        imapServerName = "imap.gmail.com";
        emailSend = "aacctestemail1@gmail.com";
        emailSendPwd = "rzk983hgs9e3";
        emailReceive = "bacctestemail1@gmail.com";
        emailReceivePwd = "few332fwef92";
        emailReceive2 = "cacctestemail1@gmail.com";
        emailReceive2Pwd = "pap235def332";
        emailReceive3 = "dacctestemail1@gmail.com";
        emailReceive3Pwd = "def111def222";

        mailConfigBean = new MailConfigBean("", emailSend, emailSendPwd, "", smtpServerName, "", "", "", "", "", "", "");
        sendAndReceive = new SendAndReceive(mailConfigBean);

        toList = new ArrayList<>();
        ccList = new ArrayList<>();
        bccList = new ArrayList<>();
        subject = "Jodd Test";
        textMsg = "Hello from plain text email: " + LocalDateTime.now();
        htmlMsg = "<html><META http-equiv=Content-Type "
                + "content=\"text/html; charset=utf-8\">"
                + "<body><h1>HTML Message</h1>"
                + "<h2>Here is some text in the HTML message</h2></body></html>";
        regularAttachments = new ArrayList<>();
        embeddedAttachments = new ArrayList<>();
    }

    @Test
    public void checkSenderInfo() throws FileNotFoundException, InvalidEmailFormatException, NoEmailFoundException {
        LOG.info("check if emails are the same");
        toList.add(emailReceive);
        Email email = sendAndReceive.send(toList, ccList, bccList, subject, textMsg, htmlMsg, regularAttachments, embeddedAttachments);
        assertEquals(mailConfigBean.getEmailAddress(), email.from().toString());
    }

    /* For the program to run peoperly you need at least one receiver which is why most part of the code has a toList.add(emailReceive) */
    @Test
    public void checkEmailSubject() throws FileNotFoundException, InvalidEmailFormatException, NoEmailFoundException {
        LOG.info("Check Email Subject:");
        toList.add(emailReceive);
        Email email = sendAndReceive.send(toList, ccList, bccList, subject, textMsg, htmlMsg, regularAttachments, embeddedAttachments);
        assertEquals(subject, email.subject());
    }

    /* For the program to run peoperly you need at least one receiver which is why most part of the code has a toList.add(emailReceive) */
    @Test
    public void multipleEmailsOneReceiver() throws FileNotFoundException, NoEmailFoundException, InvalidEmailFormatException {
        LOG.info("Check multiple emails to one person:");
        toList.add(emailReceive);
        ccList.add(emailReceive);
        bccList.add(emailReceive);
        Email email = sendAndReceive.send(toList, ccList, bccList, subject, textMsg, htmlMsg, regularAttachments, embeddedAttachments);
        assertEquals(subject, email.subject());
    }

    @Test
    public void checkEmailText() throws FileNotFoundException, InvalidEmailFormatException, NoEmailFoundException {
        LOG.info("Check Email Text:");
        toList.add(emailReceive);
        Email email = sendAndReceive.send(toList, ccList, bccList, subject, textMsg, htmlMsg, regularAttachments, embeddedAttachments);
        assertEquals(textMsg, email.messages().get(0).getContent());
    }

    @Test
    public void checkEmailHtml() throws FileNotFoundException, InvalidEmailFormatException, NoEmailFoundException {
        LOG.info("Check Email html:");
        toList.add(emailReceive);
        Email email = sendAndReceive.send(toList, ccList, bccList, subject, textMsg, htmlMsg, regularAttachments, embeddedAttachments);
        assertEquals(htmlMsg, email.messages().get(1).getContent());

    }

    @Test
    public void checkEmailRegularAttachments() throws FileNotFoundException, InvalidEmailFormatException, NoEmailFoundException {
        LOG.info("Check Email Regular Attachments:");
        toList.add(emailReceive);
        regularAttachments.add(new File("WindsorKen180-TEST.jpg"));
        Email email = sendAndReceive.send(toList, ccList, bccList, subject, textMsg, htmlMsg, regularAttachments, embeddedAttachments);
        assertEquals("WindsorKen180-TEST.jpg", email.attachments().get(0).getEncodedName());
    }

    @Test
    public void checkEmailEmbeddedAttachments() throws FileNotFoundException, InvalidEmailFormatException, NoEmailFoundException {
        LOG.info("Check Email Embdedded Attachments:");
        toList.add(emailReceive);
        embeddedAttachments.add(new File("FreeFall-TEST.jpg"));
        htmlMsg = "<html><META http-equiv=Content-Type "
                + "content=\"text/html; charset=utf-8\">"
                + "<body><h1>Here is my photograph embedded in "
                + "this email.</h1><img  width=100 height=100 id=\"1\" src='cid:FreeFall-TEST.jpg'>"
                + "<h2>I'm flying!</h2></body></html>";
        Email email = sendAndReceive.send(toList, ccList, bccList, subject, textMsg, htmlMsg, regularAttachments, embeddedAttachments);
        assertEquals("FreeFall-TEST.jpg", email.attachments().get(0).getContentId());
    }

    @Test
    public void multipleReceiversTo() throws FileNotFoundException, InvalidEmailFormatException, NoEmailFoundException {
        LOG.info(" Multiple Receivers of email using To:");
        toList.add(emailReceive);
        toList.add(emailReceive2);
        toList.add(emailReceive3);
        Email email = sendAndReceive.send(toList, ccList, bccList, subject, textMsg, htmlMsg, regularAttachments, embeddedAttachments);
        assertTrue(toList.toString().contentEquals(Arrays.toString(email.to())));
    }

    @Test
    public void multipleReceiversCc() throws FileNotFoundException, InvalidEmailFormatException, NoEmailFoundException {
        LOG.info(" Multiple Receivers of email using CC:");
        ccList.add(emailReceive);
        ccList.add(emailReceive2);
        ccList.add(emailReceive3);
        Email email = sendAndReceive.send(toList, ccList, bccList, subject, textMsg, htmlMsg, regularAttachments, embeddedAttachments);
        assertTrue(ccList.toString().contentEquals(Arrays.toString(email.cc())));
    }

    @Test
    public void multipleReceiversBcc() throws FileNotFoundException, InvalidEmailFormatException, NoEmailFoundException {
        LOG.info(" Multiple Receivers of email using BCC:");
        bccList.add(emailReceive);
        bccList.add(emailReceive2);
        bccList.add(emailReceive3);
        Email email = sendAndReceive.send(toList, ccList, bccList, subject, textMsg, htmlMsg, regularAttachments, embeddedAttachments);
        assertTrue(bccList.toString().contentEquals(Arrays.toString(email.bcc())));

    }

    @Test
    public void noReceivers() throws FileNotFoundException, InvalidEmailFormatException, NoEmailFoundException {
        thrown.expect(NoEmailFoundException.class);
        LOG.info(" No receivers of messages (null):");
        //toList.add(emailReceive);
        Email email = sendAndReceive.send(toList, ccList, bccList, subject, textMsg, htmlMsg, regularAttachments, embeddedAttachments);
    }

    @Test
    public void invalidAuthenticationFromSender() throws FileNotFoundException, InvalidEmailFormatException, NoEmailFoundException {
        thrown.expect(MailException.class);

        LOG.info(" Invalid authentication info (null):");
        //this also works if the error is in the email because the smtp server authentication checks if both are correct or it doesn't work
        mailConfigBean = new MailConfigBean("", emailSend, emailSendPwd + "error", "", smtpServerName, "", "", "", "", "", "", "");
        sendAndReceive = new SendAndReceive(mailConfigBean);
        toList.add(emailReceive);
        Email email = sendAndReceive.send(toList, ccList, bccList, subject, textMsg, htmlMsg, regularAttachments, embeddedAttachments);
    }

    @Test
    public void invalidEmailFormatofSender() throws FileNotFoundException, InvalidEmailFormatException, NoEmailFoundException {
        thrown.expect(InvalidEmailFormatException.class);

        LOG.info(" Invalid Email format of sender (null):");

        mailConfigBean = new MailConfigBean("", "NotAnEmail", emailSendPwd, "", smtpServerName, "", "", "", "", "", "", "");
        sendAndReceive = new SendAndReceive(mailConfigBean);
        toList.add(emailReceive);
        Email email = sendAndReceive.send(toList, ccList, bccList, subject, textMsg, htmlMsg, regularAttachments, embeddedAttachments);
    }

    @Test
    public void invalidEmailFormatOfReceiver() throws FileNotFoundException, InvalidEmailFormatException, NoEmailFoundException {
        thrown.expect(InvalidEmailFormatException.class);
        LOG.info(" Invalid Email format of receiver (null):");
        mailConfigBean = new MailConfigBean("", emailSend, emailSendPwd, "", smtpServerName, "", "", "", "", "", "", "");
        sendAndReceive = new SendAndReceive(mailConfigBean);
        toList.add("NotAnEmail");
        Email email = sendAndReceive.send(toList, ccList, bccList, subject, textMsg, htmlMsg, regularAttachments, embeddedAttachments);
    }

    @Test
    public void invalidRegularAttachment() throws FileNotFoundException, InvalidEmailFormatException, NoEmailFoundException {
        thrown.expect(FileNotFoundException.class);

        LOG.info("Invalid regular attachment:");
        toList.add(emailReceive);
        regularAttachments.add(new File("WindsorKen180.ERROR"));
        Email email = sendAndReceive.send(toList, ccList, bccList, subject, textMsg, htmlMsg, regularAttachments, embeddedAttachments);
    }

    @Test
    public void invalidEmbeddedAttachment() throws FileNotFoundException, InvalidEmailFormatException, NoEmailFoundException {
        thrown.expect(FileNotFoundException.class);
        LOG.info("Invalid embedded attachment:");
        toList.add(emailReceive);
        embeddedAttachments.add(new File("FreeFall.ERROR"));
        Email email = sendAndReceive.send(toList, ccList, bccList, subject, textMsg, htmlMsg, regularAttachments, embeddedAttachments);
    }

    //
    //
    //
    //TESTING RECIVING EMAILS
    //Try catches are used to make sure the emails are sent and registered before receiving them
    @Test
    public void EmailsReceivedTo() throws FileNotFoundException, InvalidEmailFormatException, NoEmailFoundException {
        LOG.info("Checking if 5 emails are received properly using to");
        toList.add(emailReceive);
        //send 5 emails
        for (int i = 0; i < 5; i++) {
            Email email = sendAndReceive.send(toList, ccList, bccList, subject, textMsg, htmlMsg, regularAttachments, embeddedAttachments);
        }
        sleeping();
        ReceivedEmail[] receivedEmails = sendAndReceive.receiveEmail(new MailConfigBean("", emailReceive, emailReceivePwd, imapServerName, "", "", "", "", "", "", "", ""));
        //use a count to compare the last 5 emails. To get latest emails, need to search the end of the [] 
        int count = 0;
        int result = 0;
        for (ReceivedEmail email : receivedEmails) {
            if (count > receivedEmails.length - 6) {
                //compare if the emails of the sender is the same for the receiver 5 times
                //if it is , add to the results which will be compared with expected
                if (mailConfigBean.getEmailAddress().contentEquals(email.from().toString())) {
                    result++;
                }
            }
            count++;
        }
        assertEquals(5, result);
    }

    @Test
    public void EmailsReceiveCC() throws FileNotFoundException, InvalidEmailFormatException, NoEmailFoundException {
        LOG.info("Checking if 5 emails are received properly using cc");
        ccList.add(emailReceive2);
        //send 5 emails
        for (int i = 0; i < 5; i++) {
            Email email = sendAndReceive.send(toList, ccList, bccList, subject, textMsg, htmlMsg, regularAttachments, embeddedAttachments);
        }
        sleeping();
        ReceivedEmail[] receivedEmails = sendAndReceive.receiveEmail(new MailConfigBean("", emailReceive2, emailReceive2Pwd, imapServerName, "", "", "", "", "", "", "", ""));
        //use a count to compare the last 5 emails. To get latest emails, need to search the end of the [] 
        int count = 0;
        int result = 0;
        for (ReceivedEmail email : receivedEmails) {
            if (count > receivedEmails.length - 6) {
                //compare if the emails of the sender is the same for the receiver 5 times
                //if it is , add to the results which will be compared with expected
                if (mailConfigBean.getEmailAddress().contentEquals(email.from().toString())) {
                    result++;
                }
            }
            count++;
        }
        assertEquals(5, result);
    }

    @Test
    public void EmailsReceiveBCC() throws FileNotFoundException, InvalidEmailFormatException, NoEmailFoundException {
        LOG.info("Checking if 5 emails are received properly using cc");
        bccList.add(emailReceive3);
        //send 5 emails
        for (int i = 0; i < 5; i++) {
            Email email = sendAndReceive.send(toList, ccList, bccList, subject, textMsg, htmlMsg, regularAttachments, embeddedAttachments);
        }
        sleeping();
        ReceivedEmail[] receivedEmails = sendAndReceive.receiveEmail(new MailConfigBean("", emailReceive3, emailReceive3Pwd, imapServerName, "", "", "", "", "", "", "", ""));
        //use a count to compare the last 5 emails. To get latest emails, need to search the end of the [] 
        int count = 0;
        int result = 0;
        for (ReceivedEmail email : receivedEmails) {
            if (count > receivedEmails.length - 6) {
                //compare if the emails of the sender is the same for the receiver 5 times
                //if it is , add to the results which will be compared with expected
                if (mailConfigBean.getEmailAddress().contentEquals(email.from().toString())) {
                    result++;
                }
            }
            count++;
        }
        assertEquals(5, result);
    }

    @Test
    public void receiveMultipleEmailsDifferentMethods() throws FileNotFoundException, InvalidEmailFormatException, NoEmailFoundException {
        LOG.info("Checking if emails are received with all the methods differently");
        toList.add(emailReceive);
        ccList.add(emailReceive);
        bccList.add(emailReceive); //cannot check bbcs but can be checked in gmail
        Email email = sendAndReceive.send(toList, ccList, bccList, subject, textMsg, htmlMsg, regularAttachments, embeddedAttachments);
        sleeping();
        ReceivedEmail[] receivedEmails = sendAndReceive.receiveEmail(new MailConfigBean("", emailReceive, emailReceivePwd, imapServerName, "", "", "", "", "", "", "", ""));
        //use a count to compare the last 5 emails. To get latest emails, need to search the end of the [] 
        int count = 0;
        //check if last email was sent in one emailto the right person
        if (receivedEmails[receivedEmails.length - 1].to()[0].getEmail().contentEquals(emailReceive)) {
            count++;
        }
        //check if last email was sent in one email to the right person
        if (receivedEmails[receivedEmails.length - 1].cc()[0].getEmail().contentEquals(emailReceive)) {
            count++;
        }
        //the count should be to because the received email can ony see the to, cc but not bcc
        assertEquals(2, count);
    }

    @Test
    public void receiveSubject() throws FileNotFoundException, InvalidEmailFormatException, NoEmailFoundException {
        LOG.info("Checking if subject of emails is correct");

        toList.add(emailReceive);
        subject = "new subject test";
        Email email = sendAndReceive.send(toList, ccList, bccList, subject, textMsg, htmlMsg, regularAttachments, embeddedAttachments);
        sleeping();
        ReceivedEmail[] receivedEmails = sendAndReceive.receiveEmail(new MailConfigBean("", emailReceive, emailReceivePwd, imapServerName, "", "", "", "", "", "", "", ""));
        assertEquals(subject, receivedEmails[receivedEmails.length - 1].subject());
    }

    @Test
    public void receiveRegularAttachments() throws FileNotFoundException, InvalidEmailFormatException, NoEmailFoundException {
        LOG.info("Checking if regular attachments are received");

        toList.add(emailReceive);
        regularAttachments.add(new File("WindsorKen180-TEST.jpg"));
        Email email = sendAndReceive.send(toList, ccList, bccList, subject, textMsg, htmlMsg, regularAttachments, embeddedAttachments);
        sleeping();
        ReceivedEmail[] receivedEmail = sendAndReceive.receiveEmail(new MailConfigBean("", emailReceive, emailReceivePwd, imapServerName, "", "", "", "", "", "", "", ""));
        assertTrue(receivedEmail[receivedEmail.length - 1].attachments().get(0).getEncodedName().contentEquals("WindsorKen180-TEST.jpg"));
    }

    @Test
    public void receiveEmbeddedAttachments() throws FileNotFoundException, InvalidEmailFormatException, NoEmailFoundException {
        LOG.info("Checking if embedded attachments are received");
        toList.add(emailReceive);
        embeddedAttachments.add(new File("FreeFall-TEST.jpg"));
        htmlMsg = "<html><META http-equiv=Content-Type "
                + "content=\"text/html; charset=utf-8\">"
                + "<body><h1>Here is my photograph embedded in "
                + "this email.</h1><img  width=100 height=100 id=\"1\" src='cid:FreeFall-TEST.jpg'>"
                + "<h2>I'm flying!</h2></body></html>";
        Email email = sendAndReceive.send(toList, ccList, bccList, subject, textMsg, htmlMsg, regularAttachments, embeddedAttachments);
        sleeping();
        ReceivedEmail[] receivedEmail = sendAndReceive.receiveEmail(new MailConfigBean("", emailReceive, emailReceivePwd, imapServerName, "", "", "", "", "", "", "", ""));
        assertTrue(receivedEmail[receivedEmail.length - 1].attachments().get(0).getEncodedName().contentEquals("FreeFall-TEST.jpg"));
    }

    //we can conclude the email is being sent properly thanks to the first test cases in this unit test and being retrived properly thanks to 
    //the receiveSubject, receiveRegularAttachment, receiveEmbeddedAttachment tests. 
    @Test
    public void invalidReceiverEmailFormat() throws InvalidEmailFormatException {
        thrown.expect(InvalidEmailFormatException.class);
        LOG.info("Checking if the format of the email is accurate");
        ReceivedEmail[] receivedEmails = sendAndReceive.receiveEmail(new MailConfigBean("", "Error", emailReceivePwd, imapServerName, "", "", "", "", "", "", "", ""));
    }

    @Test
    public void invalidReceiverAuthentication() throws InvalidEmailFormatException {
        thrown.expect(MailException.class);
        LOG.info("Checking if the user enters the proper authentication");
        //this works if there is an error in the email too since the authentication method checks both 
        ReceivedEmail[] receivedEmails = sendAndReceive.receiveEmail(new MailConfigBean("", emailReceive, emailReceivePwd + "Error", imapServerName, "", "", "", "", "", "", "", ""));
    }

    /* This method is used to sleep the program */
    public void sleeping() {
        try {
            Thread.sleep(secondsToSleep * 1000);
        } catch (InterruptedException ie) {
            Thread.currentThread().interrupt();
        }
    }
}
